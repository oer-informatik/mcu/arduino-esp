## ESP ins WLAN integrieren

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_wifi</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Mit der Bibliothek Pubsubclient ist ein ESP(32/8266) schnell an einen MQTT-Broker angebunden._

Eine einfache, aber gut konfigurierbare Bibliothek für MQTT ist der _PubSubClient_. Wie viele andere Bibliotheken findet er sich direkt in der Bibliotheksverwaltung im Menü unter `Sketch / Bibiliothek einbinden / Bibliotheken verwalten` 

![Sketch/Bibiliothek einbinden/Bibliotheken verwalten kann nach Pubsubclient gesucht werden](images/mqtt/install-pubsubclient0.png)

Es gibt relativ viele Bibliotheken, die auf PubSubClient verweisen, daher muss man bei der Suche nach `pubsub` häufig etwas scrollen, bis man den Eintrag findet. Über "Installieren" wird die Bibliotek geladen und kann dann genutzt werden.

![Bei der Suche nach "pubsub" findet sich das Ergebnis recht weit unten](images/mqtt/install-pubsubclient1.png)

Wie bei den meisten anderen Bibliotheken bringt der `PubSubClient` viele Beispiele mit, die dabei helfen, sich mit den Möglichkeiten der Bibliothek zurechtzufinden. In unserem Fall ist das Beispiel `mqtt_esp8266` eine gute Ausgangsposition für ein erstes lauffähiges Programm.

![Die mitgelieferten Beispiele finden sich im Menü Datei/Beispiele/PubSubClient - z.B. "mqtt_esp8266"](images/mqtt/install-pubsubclient2.png)



```c++
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.

const char* ssid = "........";
const char* password = "........";
const char* mqtt_server = "broker.mqtt-dashboard.com";
```



### Weitere Literatur und Quellen

- Umgang mit (WLAN-)Passwörtern in Arduino-Dateien: [Andrea Grandi: How to safely store Arduino secrets](https://www.andreagrandi.it/2020/12/16/how-to-safely-store-arduino-secrets/)

