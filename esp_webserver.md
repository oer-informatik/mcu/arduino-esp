## Webserver für den ESP

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109920478571342542</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_webserver</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Ist ein ESP erst einmal in das WLAN integriert, liegt der Schritt nahe, Eingabewerte über eine Internetseite ausgeben zu lassen oder Ausgänge über Buttons auf der Internetseite zu schalten. In diesem Artikel werden die nötigen Schritte für den ESP32 und den ESP8266 beschrieben._

### Schritt eins: Die WLAN-Verbindung herstellen

First things first: bevor ein Webserver aufgesetzt werden kann, muss zunächst der ESP in ein WLAN integriert werden. Die nötigen Schritte sind [hier beschreiben](https://oer-informatik.de/esp_wifi) - die dort genannten Voraussetzungen müssen erfüllt sein, bevor wir uns an den eigentlichen Webserver wagen.

### Schritt zwei: die richtigen Bibliotheken einbinden

Die Boardtreiber für den ESP32 und ESP8266 bringen jeweils einen eigenen Webserver mit - unglücklicherweise heißen die beiden unterschiedlich (`ESP8266Webserver.h` bzw. `Webserver.h`). Wir kennen das Problem - bei den WiFi-Bilbliotheken war es genauso. Daher gibt es hier drei unterschiedliche Lösungen: Speziell für den ESP32 und den ESP8266 zugeschnittene Programmfragmente und eine generische (allgemeine) Lösung, die dann aber etwas komplizierter ist (Auswahl über die folgenden Tabs):

Vorneweg sollte angegeben werden, auf welchem Port der Webserver antwortet. Ports ermöglichen, dass unterschiedliche Programme auf dem gleichen Gerät Anfragen entgegen nehmen können. Port `80` ist der Standardport für HTTP-Websites, also wählen wir diesen:

```c++
//-------------------------------------------------------------------------------------
// Set the Port for the Webserver
//-------------------------------------------------------------------------------------

const int WEBSERVER_PORT = 80;
```

Dann kommt der spezifische Teil unterhalb der WiFi-Bibliotheken (aus dem vorigen Tutorial) muss nun der jeweilige Webserver eingebunden werden:

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="allgemein" onclick="openTabsByDataAttr('allgemein', 'mcu')">allgemeine Lösung</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

```cpp

#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;

#include <ESP8266WebServer.h> // aktivieren für ESP8266
ESP8266WebServer server(WEBSERVER_PORT);
```

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">

```cpp

#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;

#include <WebServer.h>
WebServer server(WEBSERVER_PORT);
```

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="allgemein"  style="display:none">

Wenn das Programm sowohl auf dem ESP32 als auch auf dem ESP8266 laufen soll, dann muss man tricksen: mithilfe einer Compiler-Direktive (`#if defined...`) werden die korrekten Bibliotheken ausgewählt:

```cpp
#if defined(ESP8266)
#pragma message "Compiling Libraries for ESP8266-based boards"

#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;

#include <ESP8266WebServer.h> // aktivieren für ESP8266
ESP8266WebServer server(WEBSERVER_PORT);

#elif defined(ESP32)
#pragma message "Compiling Libraries for ESP32-based boards"
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;

#include <WebServer.h>
WebServer server(WEBSERVER_PORT);

#else
#error "Nor ESP32 or ESP8266 recognized - you have to choose the libraries manually"
#endif
```
</span>



#### Schritt drei: die Ergänzungen in der `setup()`

```cpp
void setup(){

  /* ...
  hier steht der bisherige WiFi-Code 
  ...
  danach:*/
  
  Serial.println("Starte Webserver...");
  server.on("/", homepage);
  String url = "http://"+WiFi.localIP().toString() + ":" + String(WEBSERVER_PORT);
  Serial.println("Der Server ist erreichbar über: "+url );
  server.begin();
  Serial.println("Finished startup.");
}
```

#### Schritt vier: die Ergänzungen in der `loop()`

```cpp
void loop(){
  ensureWIFIConnection(); 
  server.handleClient(); 
}
```
#### Schritt fünf: die neuen benötigten Funktionen

```cpp
String renderHtml(String header, String body) {
  // HTML & CSS contents which display on web server
  String html = "";
  html = html + "<!DOCTYPE html>\n<html>\n"+"<html lang='de'>\n<head>\n<meta charset='utf-8'>\n<title>"+header+"</title>\n</head><body>\n<h1>";
  html = html + header + "</h1>\n";
  html = html + body + "\n</body>\n</html>\n";
  return html;
}

void homepage() {
  String header = "Meine erste ESP-Homepage";
  String body = "";
  body = "<h2>Hier kann alles mögliche stehen, z.B.</h2>\n";
  body = body + "dieser Text...";

  server.send(200, "text/html; charset=utf-8", renderHtml(header, body));
}```

