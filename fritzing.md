## ESP ins WLAN integrieren

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_wifi</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Mit der Bibliothek Pubsubclient ist ein ESP(32/8266) schnell an einen MQTT-Broker angebunden._

Eine gute Quelle für Fritzing-Komponenten ist:

- das [Adafruit-Repository der Fritzing-Bibliothek](https://github.com/adafruit/Fritzing-Library/tree/master/parts)