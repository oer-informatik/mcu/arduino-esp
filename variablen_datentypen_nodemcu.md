# Ein- und Ausgabepins in Variablen speichern

Bei dem vorherigen Beispiel ist es anhand des Programmcodes sehr schwer nachzuvollziehen, was die Schaltung macht, weil im Programmkontext nicht ersichtlich ist, was zum Beispiel am Pin `D8` angeschlossen ist. Folglich muss man das Gesamtprogramm kennen, um zu erfahren, was bei `digitalWrite(D8, HIGH);` passiert.

Wenn später die Schaltung angepasst werden muss, und z.B. die rote LED an `D5` angeschlossen werden muss, muss das gesamte Programm angepasst werden.

Um diese beiden Probleme zu beheben, wollen wir die Anschlusspins in Konstanten speichern, denen wir die bezeichnende Namen geben (z.B. Namen der angeschlossenen Aktoren oder Sensoren).

```c++
/*Liste der Ein- und Ausgabelemente und Deklaration der Pins:
Datentyp | Name       | Anschlusspin   | Name, Eigenschaften*/
const int LED_ROT      = D8;            // highactive
const int LED_GELB     = D7;            // highactive
const int LED_GRUEN    = D6;            // highactive

const int kurzePause = 300;
const int langePause = 3000;

void setup() {
  pinMode(LED_ROT, OUTPUT);
  pinMode(LED_GELB, OUTPUT);
  pinMode(LED_GRUEN, OUTPUT);
}

void loop() {
  //grün
  digitalWrite(LED_GRUEN, HIGH);
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, LOW);
  delay(langePause);
  digitalWrite(LED_GRUEN, LOW);

  //gelb
  digitalWrite(LED_GELB, HIGH);
  delay(kurzePause);
  digitalWrite(LED_GELB, LOW);

  //rot
  digitalWrite(LED_ROT, HIGH);
  delay(langePause);
  digitalWrite(LED_ROT, LOW);

  // rot-gelb
  digitalWrite(LED_GELB, HIGH);
  digitalWrite(LED_ROT, HIGH);
  delay(kurzePause);
  digitalWrite(LED_ROT, LOW);
  digitalWrite(LED_GELB, LOW);
}
```


## Weitere Literatur und Quellen





