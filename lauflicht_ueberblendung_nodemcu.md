## Knight-Rider Lauflicht mit Überblendung (Fading)


![Die LEDs sind nur an PWN-Pins angeschlossen](images/pwm_lauflicht.png)


```c++
int myPins[] = {3, 5, 6, 9, 10, 11, 10, 9, 6, 5, 3};


int anzahlPins = (sizeof(myPins) / sizeof(myPins[0]));

byte highPegel = 255;
byte midiPegel = 15;
byte lowPegel = 0;
byte delayTime = 400;
byte aktiveLED = 0;
byte laufrichtung = 1;

void setup(){
	for (int i = 0; i < anzahlPins; i++) {  
      pinMode(myPins[i], OUTPUT);
	}
}



void loop2(){
	for (int i = 0; i < anzahlPins; i++) {

      analogWrite(myPins[i], highPegel);

      if (i-1>0) {
      	analogWrite(myPins[i-1], midiPegel);
      }

      if (i+1<anzahlPins){
 	      analogWrite(myPins[i+1], midiPegel);
      }

      delay(delayTime);

      analogWrite(myPins[i], lowPegel);

      if (i-1>0) {
      	analogWrite(myPins[i-1], lowPegel);
      }

      if (i+1<anzahlPins){
 	      analogWrite(myPins[i+1], lowPegel);
      }
	}
}
```
