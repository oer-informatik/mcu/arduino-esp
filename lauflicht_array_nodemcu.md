## Arduino: Umgang mit Arrays


```c++
/*
 * Liste der Ein- und Ausgabelemente und Deklaration der Pins:
Datentyp | Name       | Anschlusspin   | Ein/Ausgabelement elektrotechnische Eigenschaften, Verhalten*/
const int LED_ROT      = 11;            // highaktiv
const int LED_GRUEN    = 10;            // highaktiv
const int LED_BLAU     = 9;             // highaktiv
const int LED_GELB     = 8;             // highaktiv
const int TASTER_ROT   = 5;             // Schließer mit Arduino-internen Pullup-Widerstand
const int TASTER_GRUEN = 4;             // Schließer mit Arduino-internen Pullup-Widerstand
const int TASTER_BLAU  = 3;             // Schließer mit Arduino-internen Pullup-Widerstand
const int TASTER_GELB  = 2;             // Schließer mit Arduino-internen Pullup-Widerstand

const int pause = 300;

int LEDs[] = {LED_ROT, LED_GRUEN, LED_BLAU, LED_GELB};
int anzahlLEDs = (sizeof(LEDs) / sizeof(LEDs[0]));

int melodie[] = {0, 1, 2, 3, 0, 0, 3, 3, 2, 2};
int laengeMeldodie = (sizeof(melodie) / sizeof(melodie[0]));

void setup()
{
  pinMode(LED_ROT, OUTPUT);
  pinMode(LED_GRUEN, OUTPUT);
  pinMode(LED_BLAU, OUTPUT);
  pinMode(LED_GELB, OUTPUT);
  pinMode(TASTER_ROT, INPUT_PULLUP);
  pinMode(TASTER_GRUEN, INPUT_PULLUP);
  pinMode(TASTER_BLAU, INPUT_PULLUP);
  pinMode(TASTER_GELB, INPUT_PULLUP);
}

void loop() {
    for (int i = 0; i < laengeMeldodie; i++)  {
      digitalWrite(LEDs[melodie[i]], HIGH);
      delay(pause);
      digitalWrite(LEDs[melodie[i]], LOW);
      delay(pause);
    }
    delay(pause);
  }


```
