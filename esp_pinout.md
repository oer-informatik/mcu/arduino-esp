## Nutzbare Anschlüsse an ESP32 / ESP8266 Developer-Boards

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/114099714293639592</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_pinout</span>

> **tl/dr;** _(ca. 3 min Lesezeit): Nicht alle Pins eines Microcontroller-Boards kann man nutzen: einige steuern den Startvorgang (Strapping Pins), andere sind intern mit LED, Speicher oder Buttons verbunden. Für die ESP32-Boards ESP32 Dev Kit V1, ESP32 Dev Kit V2, ESP32 Dev Kit C V4, ESP32 C6 Dev Kit N8-M, WeMos Dev Board with 0.96 inch OLED, das ESP8266-Board gibt es hier Pinouts mit den empfohlenen frei nutzbaren Pins._


Es ist generell eine gute Idee, immer den Link zum Pinout (also: der Anschlussbelegung) des vorliegenden Microcontrollers zur Hand zu haben. Das Internet ist voll davon - manchmal muss man ein bisschen suchen, bis man überhaupt weiß, welche Version das eigene Microcontroller-Board ist. 

Auf dieser Seite folgen:

- Pinouts für die von mir häufiger genutzten ESP-Boards mit Hinweisen, welche Pins sich wofür eignen
  
  - ESP32 Dev Kit V1
  
  - ESP32 Dev Kit V2
  
  - ESP32 Dev Kit C V4
  
  - ESP32 C6 Dev Kit N8-M
  
  - WeMos Dev Board with 0.96 inch OLED
  
  - ESP8266 ("NodeMCU")

- Eine allgemeine Liste der Anschlusspins und deren Funktion/Einschränkungen, um auch für andere ESP32-Boards Rückschlüsse ziehen zu können.


### Legende für die unten folgenden Pinouts:

- `GPIO` steht für _general purpose input output_ - die Zahl dahinter ist diejenige, die man in der Programmierung nutzt (also z.B. für _GPIO13_: `pinmode(13, OUTPUT);`)
- `ADC` ist der Analog-Digital-Converter: hier gibt es zwei Stück, die auf vielen Pins genutzt werden können. ADC2 kann jedoch nicht zeitgleich mit WLAN genutzt werden.
- Alle Pins haben Pulsweitenmodulation, 16 unterschiedliche Kanäle sind hier möglich
- RTC: über diese Pins kann der ESP aus den DeepSleep geweckt werden
- SP: Strapping Pins müssen beim Booten einen bestimmen Zustand haben, da sie unterschiedliche Modi aktivieren (z.B. Programm starten vs. neues Programm laden)
- `I` und `O` groß und grün hinterlegt heißt: bedenkenlos als Input bzw. Output nutzbar
- `i` und `o` klein und gelb/hellgrün hinterlegt heißt: Es gibt Einschränkungen. Erst Text lesen, dann überlegen, ob als Input bzw. Output sinnvoll nutzbar
- `pu` bzw. `pd`: an _Strapping Pins_ gibt es häufig _PullUp_ oder _PullDown_-Widerstände, damit die Startzustände vorliegen. Die Beschaltung und Benutzung dieser Pins muss das berücksichtigen.
- hellrot / hellblau hinterlegt: Spannungsversorgung
- rot hinterlegt: Finger weg! (I.d.R. Ansteuerung des internen Speichers)
- GPIO-Name grün hinterlegt: gut geeignet für jeden Zweck
- GPIO-Name gelb hinterlegt: mit Einschränkungen nutzbar, siehe Text.

Zum genauer Recherchieren ist es unerlässlich, selbst mal in die beiden Dokumente geschaut zu haben:

- [ESP32 Technical Reference Manual](https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf)

- [ESP32 Datasheet](https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf)

### ESP32 Dev Kit V1

Gutmütiges Board mit elf frei verfügbaren und vierzehn eingeschränkt nutzbare Pins, einer LED - leider häufig mit MicroUSB-Anschluss. Wie bei vielen ESP32-Boards ist es so breit, dass auf einem normalen Breadboard nur eine der beiden Pin-Leisten leicht erreichbar ist.

Board-Treiber: "ESP32 Dev Module"

Fritzing-Datei^[Mithilfe von Fritzing lassen sich komfortable Schaltpläne und Technologieschemata erstellen => Fritzing.org] z.B. aus dem GitHub-Repo von [silveirago/FritzingParts](https://github.com/silveirago/FritzingParts/raw/refs/heads/main/DOIT%20Esp32%20DevKit%20v1%20improved.fzpz) 

![Pinout des ESP32 Dev Kit V1](images/esp32/esp32_pinout_devkitv1.png)

Fehlt etwas wesentliches auf dem Pinout? Ich bin für jeden Hinweis dankbar. Wer selbst anpassen will oder ein weiteres ESP32-Board ergänzen will [findet hier die ursprüngliche Datei](https://gitlab.com/oer-informatik/mcu/arduino-esp/-/raw/main/images/esp32/esp32_pinout.xlsx).

### ESP32 Dev Kit V2

Im wesentlichen wie das obige V1-Board (elf frei verfügbare, aber fünfzehn eingeschränkt nutzbare Pins) - es wurde nur ein Pin ergänzt, der aber nur bedingt genutzt werden kann (`GPIO0`) und relativ viele Pins, die man ohnehin nicht nutzt, weil der interene Speicher angeschlossen ist (`GPIO6`- `GPIO11`) - außerdem ist die interne LED an TX angeschlossen und somit auch nicht frei programmierbar. Wie bei vielen ESP32-Boards ist es so breit, dass auf einem normalen Breadboard nur eine der beiden Pin-Leisten leicht erreichbar ist.

Board-Treiber: "ESP32 Dev Module"


![Pinout des ESP32 Dev Kit V2](images/esp32/esp32_pinout_devkitv2.png)

Fehlt etwas wesentliches auf dem Pinout? Ich bin für jeden Hinweis dankbar. Wer selbst anpassen will oder ein weiteres ESP32-Board ergänzen will [findet hier die ursprüngliche Datei](https://gitlab.com/oer-informatik/mcu/arduino-esp/-/raw/main/images/esp32/esp32_pinout.xlsx).

### ESP32 Dev Kit C V4

Etwas kompakter als das V2-Board, auch hier elf frei verfügbare und fünfzehn eingeschränkt nutzbare Pins. Neben der Bauform einziger Unterschied: es gibt keine intere LED mehr. Wie bei vielen ESP32-Boards ist es so breit, dass auf einem normalen Breadboard nur eine der beiden Pin-Leisten leicht erreichbar ist.

Board-Treiber: "ESP32 Dev Module"

![Pinout des ESP32 Dev Kit C V4](images/esp32/esp32_pinout_devkitcv4.png)

Fehlt etwas wesentliches auf dem Pinout? Ich bin für jeden Hinweis dankbar. Wer selbst anpassen will oder ein weiteres ESP32-Board ergänzen will [findet hier die ursprüngliche Datei](https://gitlab.com/oer-informatik/mcu/arduino-esp/-/raw/main/images/esp32/esp32_pinout.xlsx).

### ESP32 C6 Dev Kit N8-M

Ein etwas untypisches Board mit einem USB-C Anschluss. Ist es mit dem PC verbunden, stellt es zwei Ports zur Verfügung. Zur seriellen Verbindung mit dem Programmer und dem _Seriellen Monitor_ der Arduino-IDE eignet sich der Port **ohne** die Bezeichnung "ESP32 Family...". Das Board ist schmaler als viele ESP32-Boards, so dass auf einem normalen Breadboard beide Pin-Leisten leicht erreichbar sind!

Das Board hat sieben frei verfügbare und zehn eingeschränkt nutzbare Pins.

Board-Treiber: "ESP32C6 Dev Module"


Fritzing-Datei^[Mithilfe von Fritzing lassen sich komfortable Schaltpläne und Technologieschemata erstellen => Fritzing.org] z.B. von [tinkerdudeeeeeeee](https://forum.fritzing.org/t/import-self-made-svg-for-esp32-c6-n8-waveshare-into-fritzing-part-editor/26162/4) aus dem [Fritzing-Forum](https://forum.fritzing.org/uploads/short-url/tr24Rx2l7LDsWbHOaPwACzJ3kqy.fzpz) 



Das Board verfügt über eine interne RGB-LED, die über die Neopixel-Bibliothek angesprochen werden kann (Siehe Board-Beispielcode).

![Pinout des ESP32 C6 Dev Kit N8-M](images/esp32/esp32_pinout_c6_devkit_n8m.png)

Das Pinout entspricht nahezu dem Pinout des [Espressif-Boards ESP32 C6 Dev Kit C 1](https://docs.espressif.com/projects/esp-dev-kits/en/latest/esp32c6/esp32-c6-devkitc-1/user_guide.html).
Das Datenblatt des genutzten Microcontrollers findet sich [hier (Espressif-Seite)](https://www.espressif.com/sites/default/files/documentation/esp32-c6-wroom-1_wroom-1u_datasheet_en.pdf)

Fehlt etwas wesentliches auf dem Pinout? Ich bin für jeden Hinweis dankbar. Wer selbst anpassen will oder ein weiteres ESP32-Board ergänzen will [findet hier die ursprüngliche Datei](https://gitlab.com/oer-informatik/mcu/arduino-esp/-/raw/main/images/esp32/esp32_pinout.xlsx).

### WeMos Dev Board with 0.96 inch OLED

Das Board hat spezielle Anwendungsfälle, daher ist auch nicht weiter schlimm, dass es lediglich drei frei verfügbare und zwölf eingeschränkt nutzbare Pins hat. Das OLED-Display macht da einiges wieder gut. Bei meinem Board ist etwas seltsam, dass ich den Boot-Button zum Programmieren händisch betätigen muss. Wie bei vielen ESP32-Boards ist es so breit, dass auf einem normalen Breadboard nur eine der beiden Pin-Leisten leicht erreichbar ist.

Board-Treiber: "ESP32 Dev Module"

![Pinout des WeMos Dev Board with 0.96 inch OLED](images/esp32/esp32_pinout_wemosDevBoardOLED.png)

Fehlt etwas wesentliches auf dem Pinout? Ich bin für jeden Hinweis dankbar. Wer selbst anpassen will oder ein weiteres ESP32-Board ergänzen will [findet hier die ursprüngliche Datei](https://gitlab.com/oer-informatik/mcu/arduino-esp/-/raw/main/images/esp32/esp32_pinout.xlsx).

### NodeMCU / ESP8266

Es gibt das NodeMCU-Board auf ESP8266-Basis in unterschiedlichen Versionen. Größter Unterschied ist, dass das vermeintlich neuste Layout nicht mehr so auf ein Breadboard passt, dass man an beide Pinleisten leicht kommt - hier lohnt also etwas Recherche vor dem Kauf!

![Pinout des ESP8266 / nodeMCU-Boards](images/nodemcu/nodeMCU_ESP8266Pinout.png)

Die kleinen Schaltpläne finden sich [hier als Falstadt-Link](https://www.falstad.com/circuit/circuitjs.html?ctz=CQAgrCAMB0l3AWAnC1b0DYQEYYCY48B2IgZkgA48MUjxJwQFTwBTAWm2wCgAnECtixCBIigwZ4C8PqKzi5ODBJzw43AOaKhDQcKJYJ3AB4C8FHNgFg62BExB57AQUgmQGMHhAGfNH0gOTiDOACIAwgA6AM4AlgB2AC6svPHcAO6KCkSQ9gpuWsze5PZFIKQEUFCyyFhODLU4vg05NXACDe3Y5lUIrQBKTEjyDcNMFioIDKTQLCowYNwEdFMM2L4Va772oZAxAPQxAOIACgCSAPJC3NFMFY5E3mUlVVaJvACurBl33sRP9zwYEMP1WjimTHaQJBmTB0N+jmB1VhgKRjXhblhY3h6IhbmW4AQ9nWWBs+iw9ni-QAygAVUFdbZQ4JuUzmSSkCndRykQLE7wANxSACMEgATVjxGIAMwAP7wYqFWKwAA7UgA2ypV3DFkLWPTBJMcIAl0oAhh91Yl3AhsCxsGBSiQcDYHNyADIAUVCeBiAApheqLQBKH64R6Wbzh7xjNy3aOR1QRuz2NYgd5fJaQWxwCxjJC+MY7UgHY7nC5ufi4SggMYJ2NMVpaBMppNR5RVVmqSD2zmqfx2vk4bwAMSD0QAFuxaWbosleNxBtWLPdl53etNZuuFrI16vc7XDMOZPxGq3Gvlj+pBmCFGCEBMN+Ut-NoIsseShsIidUAMaE4lfEdewXmJWA1AgiDsA4OgJHAqxMQApoKTrZpNCQl5gPKSojEGMgsFIPt8PKXJ1whGY5k7N93CIZAmFwDpSjtN1vC9UJsH9XgAHtElDXViNICh7GIspvFNC0rSzOgMAoCwyk8Cl7h2BBS2iU5LjwWQBKIvtBNTRtKw8WTyj7GS5PufUZEGMyTKwGyXkmTdKLg98jPM7wRIs3dIDRLoDzBSz1AJNcwQLCl2h2ChVPUq5FmbHzwrWBL6KPNwgA).

Fehlt etwas wesentliches auf dem Pinout? Ich bin für jeden Hinweis dankbar. Wer selbst anpassen will [findet hier die ursprüngliche Datei](https://gitlab.com/oer-informatik/mcu/arduino-esp/-/raw/main/images/nodemcu/nodeMCU_ESP8266Pinout.xlsx).

### Generelle Funktion und Einschränkung der ESP32-Anschlusspins:

|Pin|Input|Output|Beschreibung|
|:---|:---:|:---:|:---|
|GND|||0V (GND / LOW)|
|5V / VIN|||5V (direkt per USB, vorsicht!)|
|3V3|||3,3V  (HIGH)|
|EN / RST|||pullup, RST-Button, startet ESP neu|
|NC|||not connected (keine Funktion)|
|GPIO0|pu|o|Strapping Pin: HIGH boot / LOW programm, RTC, Touch, ADC2, Boot-Button, PullUp|
|GPIO1 / TXD|x|o|UART0-TXD (USB/Serial.print()), nur Output, Bootlog-Output, DevKitV2: LED|
|GPIO2|pd|o|StrappingPin: LOW on Boot (PullDown); RTC, Touch, ADC2, DevKitV1: LED|
|GPIO3 / RXD|i|x|UART0-RXD (USB/Serial.read()), nur Input, HIGH beim Booten|
|GPIO4|i|o|RTC, Touch, ADC2, "Strapping Pin" wird ggf. zur Bootmode-Wahl genutzt|
|GPIO5|pu|o|SP: HIGH boot, SPI CS, Boot-Output, PullUp|
|GPIO6 / CLK|||wird für Flash genutzt|
|GPIO7 / SD0|||wird für Flash genutzt|
|GPIO8/ SD1|||wird für Flash genutzt|
|GPIO9 / SD2|||UART1-TX, wird für Flash genutzt|
|GPIO10 / SD3|||UART1-RX, wird für Flash genutzt|
|GPIO11 / CMD|||wird für Flash genutzt|
|GPIO12|pd|O|SP: LOW boot, JTAG-MTDI, RTC, Touch, ADC2, als Output ok, PullDown|
|GPIO13|I|O|RTC, Touch, ADC2, JTAG-MTCK
|GPIO14|I|o|SP, JTAG-MTMS, RTC, Touch, ADC2, Boot-Output
|GPIO15|pu|o|SP: HIGH bootOutput, JTAG-MTDO, RTC, Touch, ADC2, Input gegen LOW, PullUp
|GPIO16|I|o|UART2-RX  (bei  ESP32-WROVER für PSRAM)
|GPIO17|I|o|UART2-TX (  (bei  ESP32-WROVER für PSRAM)
|GPIO18|I|O|SPI SCLK
|GPIO19|I|O|SPI MISO
|GPIO20|I|O|
|GPIO21|I|O|I2C SDA (default)
|GPIO22|I|O|I2C SCL (default)
|GPIO23|I|O|SPI MOSI
|GPIO24|I|O|
|GPIO25|I|O|RTC, DAC (8Bit), ADC2
|GPIO26|I|O|RTC, DAC (8Bit), ADC2
|GPIO27|I|O|RTC, Touch, ADC2
|GPIO28|I|O|
|GPIO29|I|O|
|GPIO30|I|O|
|GPIO31|I|O|
|GPIO32|I|O|RTC, Touch, ADC1
|GPIO33|I|O|RTC, Touch, ADC1
|GPIO34|i|x|ADC1, nur Input, kein interner pull-up/pull-down
|GPIO35|i|x|ADC1, nur Input, kein interner pull-up/pull-down
|GPIO36 / VP / SP|i|x|VP, ADC1, nur Input, kein interner pull-up/pull-down
|GPIO37|i|x|ADC1, nur Input, kein interner pull-up/pull-down
|GPIO38|i|x|ADC1, nur Input, kein interner pull-up/pull-down
|GPIO39 / VN / SN|i|x|VN, ADC1, nur Input, kein interner pull-up/pull-down




