## ESP ins WLAN integrieren

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_wifi</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Mit der Bibliothek Pubsubclient ist ein ESP(32/8266) schnell an einen MQTT-Broker angebunden._


### Pumpe

Fritzing-Datei^[Mithilfe von Fritzing lassen sich komfortable Schaltpläne und Technologieschemata erstellen => Fritzing.org] z.B. aus dem [Fritzing-Forum](https://forum.fritzing.org/t/looking-for-esp32-c6-devkitc-1/24695) von [vanepp](https://github.com/uCautomation/librerias_fritzing/raw/refs/heads/main/WATER%20PUMP-fixed.fzpz) 



