## Das Bussystem Serial Periphal Interface (SPI)

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca.  min Lesezeit): Zur Kommunikation zwischen Microcontrollern (MCU) untereinander - oder mit Sensoren wird häufig der SPI-Bus verwendet: eine Vollduplex-Vier-Draht Schnittstelle, die vor allem wegen ihrer Geschwindigkeit und Einfachheit eingesetzt wird, aber am MCU für jeden Teilnehmer GPIOs belegt._



### Links und weitere Informationen


