## Externe LEDs am Arduino anschließen

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/externe_led_ansteuern_mcu</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Nach dem ["Hello World"](https://oer-informatik.de/helloworld_nodemcu) geht es an die digitalen Ausgänge: Wie können LED angeschlossen werden? Welche gibt es? Welche Vorwiderstände benötigen sie? Woran erkenne ich den Widerstandswert? Teil 2 der NodeMCU-Reihe._

Wenn wir externe LED mit dem NodeMCU ansteuern wollen, müssen wir uns zunächst mit den elektronischen Spezifikationen der LED vertraut machen. Unser NodeMCU-Board (ESP8266 oder ESP32) liefert an den einzelnen Pins `HIGH` = 3,3V oder `LOW`= 0V, somit können wir LED an einer Spannung von maximal 3,3V betreiben. Arduino Unos laufen mit 5V, hier gelten die Überlegungen analog, jedoch mit anderen Werten (siehe Tabelle unten). Aus den Kennlinien der jeweiligen LED lassen sich Arbeitspunkte mit folgenden Betriebsspannungen ablesen:  

|LED Farbe|Betriebsspannung|
|---|---|
|rot| 1,6 - 2,2 V|
|gelb| 1,9 - 2,5V|
|grün| 1,9 - 2,5V|
|blau|2,7 - 3,5 V|
|weiß|2,7 - 3,5 V|

### Vereinfachter Fall: blaue und weiße LED zunächst direkt betreiben

Die typischen Betriebsspannungen der LED liegen unterhalb von 3,3V, wir müssen uns also noch etwas überlegen, um diese zu betreiben (nächste Abschnitt). Im Fall einer blauen oder weißen LED liegen die Betriebsspannungen teilweise über 3,3V, wir könnten diese also auch direkt anschließen. Es werden hierbei jedoch relativ große Ströme fließen und die LED recht hell leuchten - für den Dauerbetrieb sollten die Überlegungen des nächsten Absatzes gelten. 

In den Beispielen gehe ich von einem NodeMCU auf ESP8266-Basis aus (daher Pin `D6`). Bei einem ESP32 gilt alles entsprechend, nur der Pin müsste anders heißen (z.B. `GPIO4`).

Wir haben zwei Möglichkeiten, die benötigten 3,3V zu erhalten:

* zwischen einem Pin (z.B. `D6`) und GND (0V), wenn wir `D6` auf `HIGH` (also 3,3V) stellen (high-active)

![Schaltplan: LED highactive direkt an 3,3V zwischen GND und D6](images/LED_an_3V3_highactive.png)

* zwischen einem Pin (z.B. `D6`) und 3V3 (3,3V), wenn wir `D6` auf `LOW` (also 0V) stellen (low-active)

![Schaltplan: LED lowactive direkt zwischen 3V3 und D6](images/LED_an_3V3_lowactive.png)

Wichtig hierbei ist, dass wir die LED mit dem Pluspol an den jeweils positiveren Pin anschließen. Der Pluspol (die Anode) ist daran zu erkennen, dass er von allem mehr hat: mehr Kragen und mehr (längeres) Beinchen. Der Minuspol der LED (Kathode) ist an der LED abgeflacht und hat ein kürzeres Beinchen. Ist die LED verkehrt herum verbunden, sperrt sie und leuchtet nicht.

![Schaltplan: LED highactive direkt an 3,3V zwischen GND und D6](images/LED-Aufbau.PNG)

### LED mit Serienwiderstand ("Vorwiderstand")

Um LEDs dauerhaft und sicher betreiben zu können, müssen wir die anliegende Spannung auf unter 3,3V mindern. Wir fügen ein zusätzliches Bauteil in den Stromkreis ein: einen Serienwiderstand. Und wir nutzen eine der Grundgesetzmäßigkeiten des elektrischen Stromkreises, das zweite Kirchhoff'sche Gesetz:

> Die Summe aller Spannungen beim Umlauf in einer geschlossenen Masche ist Null.

![LED mit Serienwiderstand (Vorwiderstand)](images/maschenregel.png)

Wir haben in unserem Stromkreis die Spannungsquelle (`D6` und `GND`) mit $U_{ges} =3,3V$, die LED (an einer roten LED sollen $U_{LED}=1,6V$ fallen) und einen noch unbekannten Serienwiderstand $R_{V}$, an dem die Spannung $U_{V}$ fällt.

Es gilt also:

$$\sum{U_i}=0 $$

Die Spannungen müssen in einem Maschendurchlauf (Pfeilrichtung) summiert werden, wenn der Pfeil gegen die Umlaufrichtung verläuft (bei $U_{ges}$) mit negativem Vorzeichen!

$$\Rightarrow  U_{V} + U_{LED} - U_{ges} = 0$$

$$\Rightarrow  U_{V}  = U_{ges} - U_{LED} = 3,3V - 1,6 V = 1,7 V$$

Am Serienwiderstand müssen also $1,7V$ fallen, damit die LED am Betriebspunkt betrieben werden kann.

Auf welcher Seite der LED der Serienwiderstand geschaltet ist, ist dabei egal. Ich habe daher zunächst nicht die geläufige irreführende Bezeichnung "Vor"widerstand verwendet. Ein Vorwiderstand kann sich also vor und hinter der LED befinden und sollte eigentlich Serienwiderstand heißen - das hat sich jedoch nicht durchgesetzt. 

Um den Widerstand dimensionieren zu können, müssen wir dem Datenblatt der LED noch die gewünschte Betriebsstromstärke entnehmen. LEDs leuchten häufig bereits ab $I_{LED} = 5 mA$ hinreichend hell, die maximale Helligkeit liegt üblicherweise bei ca. $I_{LED} = 20 mA$. Wenn LEDs nicht nur kurzzeitig, sondern dauerhaft leuchten sollen, müssen wir außerdem die maximale Stromstärke je GPIO der verwendetn Microcontroller beachten - beim ESP8266 sind es $I_{max} = 12mA$ (ESP32 und ATMega 328P vertragen auch 20mA).

Da in unverzweigten Stromkreisen die Stromstärke an jedem Punkt identisch ist, können wir diese Stromstärke auch für den Vorwiderstand annehmen.

Mit Hilfe der Gleichung für den elektrischen Widerstand können wir den benötigten Vorwiderstandswert ausrechnen:

$$ R_{V} = \frac{U_{V}}{I_{LED}} = \frac{1,7V}{0,020A} = 85 \frac{V}{A} = 85 \Omega $$


Nicht alle Widerstandswerte sind als Bauteil verfügbar, wir müssen daher auf diskrete vorhandene Werte ausweichen. Diese sollten nicht zu niedrig gewählt werden, da sonst eine höhere Stromstärke im Stromkreis fließt. Im ungünstigsten Fall können dadurch Bauteile oder Leiterbahnen zerstört werden.
Bei einem zu hoch gewählten Widerstandswert leuchtet die LED mit etwas geringerer Leuchtkraft - das ist vertretbar und unproblematisch.

Widerstände können als Bauteil gekauft werden und sind in Serien sortiert, die unterschiedlich viele Werte je Dekade (je Faktor 10) haben. Die _E3_ Reihe hat z.B. drei Werte je Dekade: $10 \Omega, 22 \Omega, 47 \Omega$ und dann in der nächsten Dekade: $100 \Omega, 220 \Omega, 470 \Omega$ usw. Bei der _E6_ Reihe sind es entsprechend sechs Werte je Dekade, bei der E12-Reihe zwölf und so weiter. Beispielhaft sind hier für die erste Dekade ($1 \Omega <= R < 10 \Omega$) die Werte der E-Reihen genannt:

![Widerstandswerte - Übersicht der E-Reihen E3 - E24](images/Widerstandsreihen.png)


Oft liegen nur die Widerstände der E3-Reihe zwischen $100 \Omega$ und $10 k\Omega$ vor. Bei NodeMCU-Controllern kann dann für alle LED-Farben zum $100 \Omega$-Widerstand gegriffen werden (bei blauen und weißen LED kann ggf. auf diesen verzichtet werden - siehe Datenblatt oder eigene Messung). Bei 5V-basierten Microcontrollern wie dem Arduino wäre für alle LED ein $220 \Omega$-Widerstand passend. Wenn präzisere E-Reihen vorliegen, können die Widerstände anhand der folgenden Tabelle ausgewählt werden:

|LED Farbe|Betriebsspannung|errechneter Vorwiderstandswert an 3,3V Microcontroller (z.B: NodeMCU), ausgewählte Widerstände je E-Reihe|errechneter Vorwiderstandswert an 5 V Microcontroller (z.B: Arduino), ausgewählte Widerstände je E-Reihe|
|---|---|---|---|
|rot| 1,6 - 2,2 V| $55 \Omega - 85 \Omega$<br>E3/E6/E12: $100 \Omega$<br>E24: $91 \Omega$|$140 \Omega - 170 \Omega$ <br>E3/E6: $220 \Omega$<br>E12/E24: $180 \Omega$|
|gelb| 1,9 - 2,5V| $40 \Omega - 70 \Omega$<br>E3/E6: $100 \Omega$<br>E12/E24: $82 \Omega$|$125 \Omega - 155 \Omega$ <br>E3/E6: $220 \Omega$<br>E12/E24: $180 \Omega$|
|grün| 1,9 - 2,5V| $40 \Omega - 70 \Omega$<br>E3/E6: $100 \Omega$<br>E12/E24: $82 \Omega$|$125 \Omega - 155 \Omega$ <br>E3/E6: $220 \Omega$<br>E12/E24: $180 \Omega$|
|blau|2,7 - 3,5 V| $0 \Omega - 30 \Omega$<br>E3: $47 \Omega$<br>E6/E12/E24: $33 \Omega$|$75 \Omega - 125 \Omega$ <br>E3: $220 \Omega$<br>E6/E12/E24: $150 \Omega$|
|weiß|2,7 - 3,5 V| $0 \Omega - 30 \Omega$<br>E3: $47 \Omega$<br>E6/E12/E24: $33 \Omega$|$75 \Omega - 125 \Omega$ <br>E3: $220 \Omega$<br>E6/E12/E24: $150 \Omega$|

Da die Widerstände als Bauteil zu klein sind, um Werte darauf aufzudrucken, wurde eine Farbcodierung entwickelt. Vier oder fünf Farbringe beschreiben den jeweiligen Wert:

- Bei vier Ringen: zwei Zahlenwerte, ein Multiplikator, ein Ring zur Angabe der Toleranz (silber oder gold).

- Bei fünf Ringen: drei Zahlenwerte, ein Multiplikator, ein Ring zur Angabe der Toleranz (grün, blau, violett).

![Farbcodierung von Widerstandswerten](images/Widerstandswerte.png)


Ein $100 \Omega (5\%)$ Widerstand mit vier Ringen hat die Farbfolge: braun-schwarz-braun-gold. Weitere verbreitete Beispiele mit 4 Ringen:

![Farbcodierung von Widerstandswerten](images/Widerstandsringe-Beispiel-4Ring.png)

Ein $100 \Omega (1\%)$ Widerstand mit fünf Ringen hat die Farbfolge: braun-schwarz-schwarz-schwarz-braun. Weitere verbreitete Beispiele mit 5 Ringen:

![Farbcodierung von Widerstandswerten](images/Widerstandsringe-Beispiel-5Ring.png)


### Aufbau der Schaltung mit externer LED auf einem Breadboard

Das Breadboard führt oben und unten je eine blaue Schiene für Ground (`GND`) und eine rote Schiene für 3,3 Volt (`3V3`), die waagerecht durchkontaktiert sind und mit den entsprechenden Anschlüssen des NodeMCU verbunden werden sollten.

In der Mitte des Breadboards sind oberhalb und unterhalb der mittigen Nut jeweils 5 Pins senkrecht durchkontaktiert.

Wir schließen die LED an, in dem wir von der unteren `GND`-Schiene kommend auf eine Spalte  der unteren Hälfte gehen, von dort über einen $100 \Omega$ - Widerstand auf die obere Hälfte, dort in derselben Spalte die Kathode der LED anschließen (das kürzere Beinchen). Die Anode der LED steckt in der benachbarten Spalte, von wo eine Brücke zu einem der gewählten digitalen Ausgänge geht. Der Beispielcode hier ist für einen ESP8266-basierten NodeMCU (`D6`, `D7` und `D8`). Für andere Microcontroller müssen einfach die entsprechenden GPIO-Bezeichnungen ersetzt werden (also z.B. `GPIO25`, `GPIO26`, `GPIO27`).

![Aufbau einer Ampel mit dem NodeMCU und den Anschlüssen D6, D7 und D8](images/AmpelschaltungFritzing.png)

Ein zugehöriger Sketch nutzt dieselben Elemente, die wir schon beim "Hello World" kennengelernt hatten - nur mit den gewählten Anschlüssen der LED. Diesmal sind die LED _highactive_, leuchten also bei anliegendem `HIGH`-Signal. Eine Ampelschaltung könnte etwa so aussehen:


```cpp
/*Liste der Ein- und Ausgabeelemente und Deklaration der Pins:
Datentyp | Name       | Anschlusspin   | Name, Eigenschaften*/
const int LED_ROT      = D8;            // highaktiv
const int LED_GELB     = D7;            // highaktiv
const int LED_GRUEN    = D6;            // highaktiv


void setup() {
  pinMode(LED_GRUEN, OUTPUT);    // define LED-Pins as an Output
  pinMode(LED_GELB, OUTPUT);     
  pinMode(LED_ROT, OUTPUT);      
}

void loop() {
  //grün
  digitalWrite(LED_GRUEN, HIGH);
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, LOW);
  delay(3000);              // wait for three seconds (3000ms)

  //gelb
  digitalWrite(LED_GRUEN, LOW);
  digitalWrite(LED_GELB, HIGH);
  delay(1000);              // wait for a second (1000ms)

  //rot
  digitalWrite(LED_GELB, LOW);
  digitalWrite(LED_ROT, HIGH);
  delay(3000);              // wait for three seconds (3000ms)

  // rot-gelb
  digitalWrite(LED_GELB, HIGH);
  digitalWrite(LED_ROT, HIGH);
  delay(1000);              // wait for a second (1000ms)
}
```
