## Temperatur- und Drucksensoren BMP280 und BME280 an Microcontrollern per SPI oder I2C betreiben

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_bme280</span>

> **tl/dr;** _(ca. 5 min Lesezeit): BMP280 und BME280 sind günstige Temperatur- und Drucksensoren, die sich mithilfe vieler Bibliotheken an Microcontrollern betreiben lassen. Es die unterschiede der Sensoren und deren Anschluss per SPI und I2C gezeigt. Einfache Beispielcodes, mögliche Fehlerquellen und wie individuelle Pins/Adressen gewählt werden können wird kurz umrissen._


### Teilabschnitte dieses Artikels sind:

- Welcher Sensor liegt vor?

- Generelle Überlegungen zu Installation und Inbetriebnahme von Devices am Microcontrollerboard

- Aufbau und Beispielcode mit der I2C-Schnittstelle

- Individuelle Konfiguration per I2C-Schnittstelle

- Aufbau und Beispielcode mit der SPI-Schnittstelle

- Individuelle Konfiguration per SPI-Schnittstelle

### Welcher Sensor liegt vor Dir? BME280 oder BMP280?

Es gibt unzählige Breakout-Boards für zwei sehr ähnlichen Temperatur/Druck Sensoren von Bosch: der BM**E**280 und der BM**P**280 sind sich in Aussehen und Funktion zum Verwecheln ähnlich. Zunächst müssen wir erst einmal herausfinden, welcher Sensor auf dem jeweiligen Board verbaut wurde: 

![Temperatur- und Drucksensoren BME280 (links, kann auch Luftfeuchtigkeit) und BMP280 (rechts)](images/bme280/bmp-bme.jpg)

Den etwas funktionsreicheren BME280-Sensor (im Bild oben links) erkennt man bei genauem Hinsehen am eher quadratischeren Metallgehäuse, das eine winzige Öffnung fast mittig zu linken Seitenkante hat (gut zu erkennen auf den [Datenblattseiten des Herstellers Bosch](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/)). Der BME280 kann neben Temperatur und Luftdruck auch noch die Luftfeuchtigkeit messen.

Demgegenüber hat der BMP280-Sensor (im Bild oben rechts) einen rechteckigen Sensor mit unterschiedlichen langen Kanten. Er ist etwas günstiger und kann nicht die Luftfeuchtigkeit messen. Das kleine Metallgehäuse hat eine Öffnung, die eher in der Ecke liegt (gut zu erkennen z.B. auch auf den [Datenblattseiten des Herstellers Bosch](https://www.bosch-sensortec.com/products/environmental-sensors/pressure-sensors/bmp280/)).


### Generelle Überlegungen zu Installation und Inbetriebnahme von Devices am Microcontrollerboard

Das Vorgehen ist eigentlich bei allen Geräten im Arduino-Kosmos identisch, unabhängig davon, ob es ein Sensor, ein Aktor, ein Display oder etwas völlig anderes ist:

1. **Typenbezeichnung suchen**: Zuerst muss die Art und Typ des neuen Geräts herausgefunden werden. Manchmal steht dies auf der Platine, manchmal muss man im Netz nach Fotos/Pinouts suchen (siehe unten). 

2. **Bibliothek wählen und installieren**: Wird mit der Arduino-IDE, so muss im Menü `Werkzeuge` / `Bibliotheken verwalten` eine passende Bibliothek für das Gerät gesucht werden. Häufig sind die Bibliotheken von Adafruit relativ gut.

3. **Pins des Geräts recherchieren**: Zum Anschließen des Geräts benötigen wir die Pinbezeichnungen der jeweils genutzten Platine. Nicht immer ist der Aufdruck auf den Platinen komplett, häufig wirkt ein zusätzliches Pinout (Bildersuche nach "Pinout _Gerätebezeichnung_") Wunder.

4. **Freie Pins des Microcontrollers wählen**: Bevor wir verdrahten können, sollten wir uns noch entscheiden, welche Schnittstelle wir verwenden wollen (wenn das Gerät mehrere anbietet - z.B. SPI oder I2C). Dazu benötigen wir eine Liste der freien Pins unseres Microcontrollers, der für diese Schnittstelle geeignet ist (für [einige ESPs beispielsweise in diesem Artikel](https://oer-informatik.de/esp_pinout). Ggf. hilft auch ein Blick in die Beispielquelltexte mit Hinweisen (s.u.).

5. **Verdrahten und mit Beispielcode erkunden**: Dann kann das Gerät verdrahtet werden und über Beispiel-Quelltexte (`Datei`/ `Beispiele` häufig ganz unten: "Beispiele angepasster Bibliotheken") erkundet werden.

In unserem konkreten Fall:

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="sensor" data-tabid="bme280" onclick="openTabsByDataAttr('bme280', 'sensor')">BME280</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="bmp280" onclick="openTabsByDataAttr('bmp280', 'sensor')">BMP280</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="all" onclick="openTabsByDataAttr('all', 'sensor')">_all_</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="none" onclick="openTabsByDataAttr('none', 'sensor')">_none_</button>
</span>

<span class="tabs" data-tabgroup="sensor" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sensor" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sensor" data-tabid="bme280">

Zu (1) **Typenbezeichnung suchen**: BME280 - siehe oben

Zu (2) **Bibliothek wählen und installieren**:  `Werkzeuge` / `Bibliotheken verwalten` 

Die Suche nach "BME280" liefert u.a. die Bibliothek "Adafruit BME280 Library", die ich in dem Beispiel nutze - andere Bibiotheken können aber ebenso eingesetzt werden:

![Installieren der Bibliothek "Adafruit BME280 Library"](images/bme280/bme280library.png)

Die Bibliothek benötigt eine Reihe von weiteren Abhängigkeiten, die direkt mit installiert werden können:

![BME280 ist abhängig von BusIO und Unified Sensor](images/bme280/bme280dependency.png)

</span>

<span class="tabs"  data-tabgroup="sensor" data-tabid="bmp280" style="display:none">


Zu (1) **Typenbezeichnung suchen**: BMP280 - siehe oben

Zu (2) **Bibliothek wählen und installieren**:  `Werkzeuge` / `Bibliotheken verwalten` 

Die Suche nach "BMP280" liefert u.a. die Bibliothek "Adafruit BMP280 Library", die ich in dem Beispiel nutze - andere Bibiotheken können aber ebenso eingesetzt werden:

![Installieren der Bibliothek "Adafruit BMP280 Library"](images/bme280/bmp280library.png)

Die Bibliothek benötigt eine Reihe von weiteren Abhängigkeiten, die direkt mit installiert werden können:

![BME280 ist abhängig von BusIO und Unified Sensor](images/bme280/bmp280dependency.png)

</span>


###  Aufbau und Beispielcode mit der I2C-Schnittstelle

Zu (3) **Pins des Geräts recherchieren**

Ich schließe den BME280/BMP280 zunächst per I2C-Schnittstelle an - hierfür ist neben der Spannungsversorgung (`3V3`, `GND`) noch der Clock (`SCL`) und Datenanschluss (`SDA`) nötig. Die beiden übrigen Pins (`CSB`, `SDO`) sind nur für den Anschluss per SPI-Schnittstelle nötig (oder zur Änderung der I2C-Adresse), können daher unbeschaltet bleiben.

Zu (4) **Freie Pins des Microcontrollers wählen**

Da wir den BME280/BMP280 per I2C-Schnittstelle anschließen wollen, nutzen wir einfach die vorgesehenen Pins:

- bei ESP32 ist dies: `GPIO21` für `SDA` und `GPIO22` für `SCL`. 

- bei ESP8266 ist dies: `D2` für `SDA` und `D1` für `SCL`. 

- bei Arduino Uno ist dies: `A4` für `SDA` und `A5` für `SCL`. 

Es wären auch andere Pins möglich (siehe unten), aber so sparen wir uns etwas Konfigurationsarbeit.

Zu (5) **Verdrahten und mit Beispielcode erkunden**: 

Der Aufbau sieht bei mir mit einem ESP32 Dev V1 so aus (vorsicht: ggf. ist die Pinleiste andersherum aufgelötet und alle Pins gespiegelt): 

![Beispiel der Verdrahtung an einem ESP32 Dev V1](images/bme280/ep32devv1-bme280-i2c.png)

Natürlich müssen bei einem anderen Microcontroller auch andere Pins verwendet werden, da die Bibliothek die Adafruit_BMx280-Bibliothek die I2C-Standardpins der jeweiligen Boards verwendet. (Wie man von den Standardports abweichen kann erkläre ich weiter unten).

Wichtig ist zu beachten, dass die Pinanordnung des Sensors auch spiegelverkehrt sein kann, wenn die Pin-Leiste auf der anderen Seite angelötet wurde. Außerdem sind die Spannungsversorgungs-Schienen (rote und blaue Pinleisten außen) am Breadboard manchmal umgekehrt - also bitte immer alle Kontakte des eigenen Aufbaus prüfen und nicht einfach nach optischer Ähnlichkeit zusammenstecken!

Die I2C-Schnittstelle identifiziert Geräte am Bus über eine eindeutige Adresse. Jedes Gerät erkennt anhand dieser Adresse, wer Empfänger der Nachrichten am Bus sein soll. Die Adresse umfasst sieben Bit, es gibt also $2^7 = 128$ verschiedene Adressen ( also `0` und `127` dezimal, `0x00`-`0x7F` hexadezimal, `0b0000000` - `0b1111111` binär). Damit mehrere Geräte des gleichen Typs angeschlossen werden können, wird häufig nur ein Teil dieser Adresse durch den Hersteller fest vorgegeben, einige Bit können manuel konfiguriert werden. Bei den BMx280-Sensoren wird das letzte Bit angepasst - es können die  I2C-Adressen `0x76` (binär: `0b1110110`) oder `0x77` (binär: `0b1110111`) konfiguriert werden. Im Fall der oben abgebildeten BMx280-Breakout-Boards wird das letzte Bit über den Pin `SDO` bestimmt: liegt hier ein `HIGH` an, reagiert der Sensor auf die Adresse `0x77`, ist der Pin nicht verdrahtet oder `LOW`, dann regiert der Sensor auf `0x76`. 

Die genutzte Bibliothek spricht standardmäßig die Adresse `0x77` an. Wir müssen also entweder ein `HIGH` an `SDO` anlegen - oder aber die Software an die neue Adresse (`0x76`) anpassen (wie unten geschehen).   

Ein minimales Programm, das den BMx280-Sensor mit der Adafruit-Bibliothek abfragt, könnte also so aussehen:

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="sensor" data-tabid="bme280" onclick="openTabsByDataAttr('bme280', 'sensor')">BME280</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="bmp280" onclick="openTabsByDataAttr('bmp280', 'sensor')">BMP280</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="all" onclick="openTabsByDataAttr('all', 'sensor')">_all_</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="none" onclick="openTabsByDataAttr('none', 'sensor')">_none_</button>
</span>

<span class="tabs" data-tabgroup="sensor" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sensor" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sensor" data-tabid="bme280">

```c++
#include <Adafruit_BME280.h>

Adafruit_BME280 bme; 
unsigned i2cAddress = 0x76; //try 0x77, if this is not working for you

void setup(){
  Serial.begin(115200);
  if (!bme.begin(i2cAddress)) {Serial.println("Could not find a valid BME280 sensor, check wiring, i2c address");}
  Serial.println("Temperature | Pressure   | Altitude | Humidity |");
}

void loop() { 
  char textBuffer[52];
  sprintf(textBuffer, "    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |", bme.readTemperature(), (bme.readPressure() / 100.0F), bme.readAltitude(1013.25), bme.readHumidity());
  Serial.println(textBuffer);
  delay(1000);
}
```
</span>

<span class="tabs"  data-tabgroup="sensor" data-tabid="bmp280" style="display:none">

```c++
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bmp; 
unsigned i2cAdress = 0x76; //try 0x77, if this is not working for you

void setup(){
  Serial.begin(115200);
  if (!bmp.begin(i2cAdress)) {Serial.println("Could not find a valid BME280 sensor, check wiring, i2c address");}
  Serial.println("Temperature | Pressure   | Altitude |");
}

void loop() { 
  char textBuffer[48];
  sprintf(textBuffer, "    %2.1f °C | %4.1f hPa |   %3.1f m |", bmp.readTemperature(), (bmp.readPressure() / 100.0F), bmp.readAltitude(1013.25));
  Serial.println(textBuffer);
  delay(1000);
}
```

</span>

Der seriellen Monitor (`Strg`+ `Shift`+`M`) sollte bei `115200 Baud` eingestellt jetzt folgendes ausgeben (beim BME zusätzlich noch die Spalte "Humidity"):

```
Temperature | Pressure   | Altitude |
    24.0 °C | 1004.1 hPa |   76.7 m |
    23.9 °C | 1004.1 hPa |   76.8 m |
    23.9 °C | 1004.1 hPa |   76.7 m |
    23.8 °C | 1004.1 hPa |   76.2 m |
    23.7 °C | 1004.1 hPa |   76.5 m |
```

Wenn dieser Programmcode oder die vom Treiber mitgelieferten Beispielsketche  (`Datei`/ `Beispiele` / ganz unten: BME280 bzw. BMP280 ) nicht gehen, dann hat das häufig die folgenden Ursachen:

- `Could not find a valid BME280 sensor, check wiring, i2c address`: Die I2C-Adresse ist falsch konfiguriert oder wird nicht erkannt. Welche Adresse hat der Sensor überhaupt und ist alles richtig verdrahtet? Hier hilft ein Scan des I2C-Buses über das Beispielprogramm: `Datei`/ `Beispiele` / `Wire`/ `WireScan`. Im Normalfall reicht es, sowohl die  `0x76` als auch die `0x77` als Adresse zu versuchen.

- Ausgabe der Werte ist unrealistisch, z.B. `0.0 °C |  0.0 hPa |   44330.0 m |`: Es handelt sich um den falschen Treiber für den Sensor - bitte nochmals prüfen, ob es ein BME280 oder BMP280 ist (siehe oben).

- Die Pins wurden vertauscht - das passiert insbesondere, wenn die Beschriftung auf der Unterseite ist und man sich alles "auf dem Kopf" vorstellen muss.

###  Indivuduelle Konfiguration per I2C-Schnittstelle

Sofern nicht die Standardports der I2C-Schnittstelle verwendet werden sollen (weil diese z.B. zwingend anderweitig vergeben werden müssen), können wir die Pins auch individuell konfigurieren.

Intern nutzt der Adafruit_BMx280-Treiber eine Arduino-Bibliothek namens `Wire`, um die I2C-Kommunikation abzuwickeln. Mit Hilfe dieser Bibliothek können wir die I2C-Verbindung auch auf andere Pins legen, indem wir zu Beginn unseres Programms die Pin-Bezeichnungen über den Aufruf `Wire.begin(MCU_SDA, MCU_SCL);` übergeben. Natürlich müssen die beiden Variablen, die ich hier mal `MCU_SDA` und `MCU_SCL` genannt habe, vorher deklariert und initialisiert werden. Alternativ können auch direkt die Pinbezeichnungen übergeben werden (`Wire.begin(18, 19);`), war aber den Quelltext schlecht wartbar machen würde.


```c++
#include <Adafruit_BME280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     MCU_SDA             = 21;               // GPIO of MCU connected to SDA-Pin of Sensor
const int     MCU_SCL             = 22;               // GPIO of MCU connected to SDCL-Pin of Sensor
const int     I2C_ADDR            = 0x76;             // if SD0 is floating or LOW 0x76, else 0x77

//-------------------------------------------------------------------------------------
// App-Settings
//-------------------------------------------------------------------------------------
#define SEALEVELPRESSURE_HPA (1013.25)
unsigned long delayTime             = 1000;
unsigned long serialMonitorBaudrate = 115200;

Adafruit_BME280 bme; 


void setup(){
  Serial.begin(serialMonitorBaudrate);        // Activate debugging via serial monitor
  
  Wire.begin(MCU_SDA, MCU_SCL);               // connection with individual I2C-Pins
  int status = bme.begin(I2C_ADDR, &Wire);    //status = bme.begin();
  
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring, i2c address");
    while (1) delay(10);
  }
  Serial.println("|Temperature | Pressure   | Altitude | Humidity |");
}


void loop() { 
    serialPrintMeasurement();
    delay(delayTime);
}


void serialPrintMeasurement() {
  char prBuffer[51]; // Print buffer
  float temp     = bme.readTemperature();
  float pressure = bme.readPressure() / 100.0F;
  float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  float humidity = bme.readHumidity();

  sprintf(prBuffer, "|    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |",temp, pressure, altitude, humidity);
  Serial.println(prBuffer);
}
```


### Aufbau und Beispielcode mit der SPI-Schnittstelle

Wenn wir an Stelle der I2C-Schnitstelle die SPI-Schnittstelle nutzen wollen, können wir die Überlegungen (1) _Sensorart bestimmen_ und (2) _Treiber wählen und installieren_ von oben übernehmen. Bei der Pin-Belegung gibt es aber Unterschiede:


Zu (3) **Pins des Geräts recherchieren**

SPI nutzt neben der Spannungsversorgung (`3V3`, `GND`) vier weitere Pins: die Clock (`SCL`), Datenausgang des Microcontrollers (`MOSI`, `SDA`), Dateneingang des Microcontrollers (`MISO`, `SDO`) und ein Pin zur Sensorauswahl (Chip Select, `CSB`). Wesentliche Unterschiede zu I2C sind also, dass wir über unterschiedliche Leitungen lesen und schreiben ("Multiplex") und anstelle einer Adressierung dem Sensor über eine eigene digitale Leitung mitgeteilt wird, dass die Nachricht am Bus für ihn bestimmt wird (`CSB`).

Zu (4) **Freie Pins des Microcontrollers wählen**

Auch die SPI-Kommunikation mit dem BME280/BMP280 wollen wir zunächst über die vorgesehenen Pins lösen:

- bei ESP32 ist dies: `GPIO18` für `SCL`,  `GPIO19` für `MISO`/`SDO`, `GPIO23` für `MOSI`/`SDA`  und   `GPIO5` für `CSB`

- bei ESP8266 ist dies: `GPIO15`/`D5` für `SCL`,  `GPIO12`/`D6` für `MISO`/`SDO`, `GPIO13`/`D7` für `MOSI`/`SDA`  und   `GPIO15`/`D8` für `CSB`

- bei Arduino Uno ist dies: `D13` für `SCL`,  `D12` für `MISO`/`SDO`, `D11` für `MOSI`/`SDA`  und   `D10` für `CSB`

Es wären auch andere Pins möglich (siehe unten), aber so sparen wir uns etwas Konfigurationsarbeit.

Zu (5) **Verdrahten und mit Beispielcode erkunden**: 

Der Aufbau sieht bei mir mit einem ESP32 Dev V1 so aus (vorsicht: ggf. ist die Pinleiste andersherum aufgelötet und alle Pins gespiegelt): 

![Beispiel der Verdrahtung an einem ESP32 Dev V1](images/bme280/ep32devv1-bme280-spi.png)


Der Adafruit-Treiber ist so generisch aufgebaut, dass sich nur wenige Codezeilen ändern, wenn statt der I2C-Schnittstelle die SPI-Schnittstelle verwendet wird. 

- Beim Aufruf des Treibers wird der _Chip Select_-Pin übergeben - damit erkennt der Treiber automatisch, dass SPI verwendet werden soll: `Adafruit_BMP280 bmp(BMP_CS);`

- Beim Starten der Messung wird keine I2C-Adresse übergeben: `bmp.begin()`

Damit sollte das Auslesen per SPI klappen. Mit unterschiedlichen `CS`-Pins können so auch einfach mehrere Sensoren/Aktoren an den SPI-Bus angeschlossen werden. Ein minimales Codebeispiel sieht so aus: 

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="sensor" data-tabid="bme280" onclick="openTabsByDataAttr('bme280', 'sensor')">BME280</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="bmp280" onclick="openTabsByDataAttr('bmp280', 'sensor')">BMP280</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="all" onclick="openTabsByDataAttr('all', 'sensor')">_all_</button>
  <button class="tablink" data-tabgroup="sensor" data-tabid="none" onclick="openTabsByDataAttr('none', 'sensor')">_none_</button>
</span>

<span class="tabs" data-tabgroup="sensor" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sensor" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sensor" data-tabid="bme280">

```c++
#include <Adafruit_BME280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     BMP_CS              = 5;                // GPIO of MCU connected to SPI-Chip-Select-(CSB)-Pin

Adafruit_BME280 bme(BMP_CS); // hardware SPI

void setup(){
  Serial.begin(115200);
  if (!bme.begin()) {Serial.println("Could not find a valid BME280 sensor, check wiring");}
  Serial.println("Temperature | Pressure   | Altitude | Humidity |");
}

void loop() { 
  char textBuffer[52];
  sprintf(textBuffer, "    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |", bme.readTemperature(), (bme.readPressure() / 100.0F), bme.readAltitude(1013.25), bme.readHumidity());
  Serial.println(textBuffer);
  delay(1000);
}
```
</span>

<span class="tabs"  data-tabgroup="sensor" data-tabid="bmp280" style="display:none">

```c++
#include <Adafruit_BMP280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     BMP_CS              = 5;                // GPIO of MCU connected to SPI-Chip-Select-(CSB)-Pin

Adafruit_BMP280 bmp(BMP_CS); // hardware SPI

void setup(){
  Serial.begin(115200);
  if (!bmp.begin()) {Serial.println("Could not find a valid BME280 sensor, check wiring");}
  Serial.println("Temperature | Pressure   | Altitude |");
}

void loop() { 
  char textBuffer[48];
  sprintf(textBuffer, "    %2.1f °C | %4.1f hPa |   %3.1f m |", bmp.readTemperature(), (bmp.readPressure() / 100.0F), bmp.readAltitude(1013.25));
  Serial.println(textBuffer);
  delay(1000);
}
```
</span>


### Individuelle Konfiguration per SPI-Schnittstelle

Manchmal stehen die Standard-SPI-Ports des Microcontrollers nicht (mehr) zur Verfügung oder müssen anderweitig genutzt werden. Die Adafruit-Bibliothek ermöglicht es auch in diesem Fall, andere Pins zu vergeben. Ein Beispielprogramm mit der Möglichkeit, alle SPI-Pins zu konfigurieren, könnte z.B. so aussehen:

```c++
#include <Adafruit_BME280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     BMP_CS              = 5;                // GPIO of MCU connected to SPI-Chip-Select-(CSB)-Pin
const int     BMP_MOSI            = 23;               // GPIO of MCU connected to SDA/SPI-MOSI-Pin
const int     BMP_MISO            = 19;               // GPIO of MCU connected to SDO/SPI-MISO-Pin
const int     BMP_CLK             = 18;               // GPIO of MCU connected to SCL/SPI-Clock-Pin

//-------------------------------------------------------------------------------------
// App-Settings
//-------------------------------------------------------------------------------------
#define SEALEVELPRESSURE_HPA (1013.25)
unsigned long delayTime             = 1000;
unsigned long serialMonitorBaudrate = 115200;

Adafruit_BME280 bme(BMP_CS, BMP_MOSI, BMP_MISO, BMP_CLK); // indivudual SPI-Pins

void setup(){
  Serial.begin(serialMonitorBaudrate);        // Activate debugging via serial monitor

  int status = bme.begin();
  
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring");
    while (1) delay(10);
  }
  Serial.println("|Temperature | Pressure   | Altitude | Humidity |");
}


void loop() { 
    serialPrintMeasurement();
    delay(delayTime);
}


void serialPrintMeasurement() {
  char prBuffer[51]; // Print buffer
  float temp     = bme.readTemperature();
  float pressure = bme.readPressure() / 100.0F;
  float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  float humidity = bme.readHumidity();

  sprintf(prBuffer, "|    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |",temp, pressure, altitude, humidity);
  Serial.println(prBuffer);
}
```

Fritzing-Datei^[Mithilfe von Fritzing lassen sich komfortable Schaltpläne und Technologieschemata erstellen => Fritzing.org] z.B. von [vanepp/MWS](https://forum.fritzing.org/t/gy-bme-p280-part/3064/6) aus dem [Fritzing-Forum](https://forum.fritzing.org/uploads/short-url/mJ1l9MJIp3WnLzEZF6kqGwhhOH3.fzpz) 








