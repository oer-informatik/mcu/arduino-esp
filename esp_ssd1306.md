## Nutzbare Anschlüsse an ESP32 Developer-Boards

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/esp_bme280</span>

> **tl/dr;** _(ca. 3 min Lesezeit): BMP280 und BME280 sind günstige Temperatursensoren._

### SSD1306

Fritzing-Datei^[Mithilfe von Fritzing lassen sich komfortable Schaltpläne und Technologieschemata erstellen => Fritzing.org] z.B. aus dem GitHub-Repo von [uCautomation /
librerias_fritzing](https://github.com/uCautomation/librerias_fritzing/raw/refs/heads/main/OLED_096_128x64_I2C_SSD1306.fzpz) 
