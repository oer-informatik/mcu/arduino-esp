## Erste Schritte mit dem Microcontroller ESP8266/NodeMCU

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/helloworld_mcu</span>

> **tl/dr;** _(ca. 12 min Lesezeit): Keine neue Programmiersprache ohne ein "Hello World", kein Microcontroller-Projekt ohne LED-Blinken. Hier geht es nur um die ersten Schritte mit dem Microcontroller-Board (ESP32, ESP8266 oder Arduino Uno): Einrichtung und zwei kleine Beispielprogramme zur Nutzung der internen LED und Taster._

Dieses Tutorial beschreibt die ersten Schritte mit einem Microcontroller-Board. Die Schritte ähneln sich bei allen Microcontrollern, jedoch gibt es Abschnitte, in denen gesondert auf den Arduino Uno, den ESP8266 (nodeMCU) oder den ESP32 eingegangen wird. Über den jeweiligen Abschnitten gibt es Registerkarten, mit denen der passende Microcontroller ausgewählt werden kann.

#### Treiber für den Microcontroller installieren

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="uno" onclick="openTabsByDataAttr('uno', 'mcu')">Arduino Uno</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

Damit der Microcontroller auf dem _NodeMCU_ erkannt wird, muss zunächst ein Treiber in der Arduino-IDE nachinstalliert werden (oder geprüft werden, ob die Einstellungen bereits vorhanden sind).

Unter `Datei/Einstellungen` (bzw. `File/Preferences`) gibt es unten den Punkt "Additional boards manager URLs", in dem (ggf. neben weiteren) auch die folgende URL eingetragen werden muss:

```
http://arduino.esp8266.com/stable/package_esp8266com_index.json
```

![In den Settings muss die URL des ESP8266 eingetragen werden](images/nodemcu/install-boardmanager-esp8266.png)

Nach dem Klick auf "ok" wird die verlinkte JSON-Datei geladen. Kann im Boardmanager der entsprechende Treiber geladen werden. Boardsmanager öffnen (`Tools/Board/BoardsManager`):

![Boards Manager öffnen (Ctrl-Shift-8)](images/nodemcu/install-boardmanager-esp8266-02.png)

Im Boards-Manager nach "ESP8266" suchen und die Boards installieren:

![Boards Manager öffnen (Ctrl-Shift-8)](images/nodemcu/install-boardmanager-esp8266-03.png)


</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">

Damit der Microcontroller auf dem _ESP32_ erkannt wird, muss zunächst ein Treiber in der Arduino-IDE nachinstalliert werden (oder geprüft werden, ob die Einstellungen bereits vorhanden sind).

Unter `Datei/Einstellungen` (bzw. `File/Preferences`) gibt es den Punkt "Additional boards manager URLs"


![Unter File/Preferences auf das Icon zu "Additional boards manager URLs klicken"](images/esp32/preferences_board_manager.png)

Es öffnet sich ein Fenster, in dem die folgende  zusätzliche URL eingetragen werden muss:

```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
```

![In den Settings muss die URL des ESP32 eingetragen werden](images/esp32/board_manager_url.png)

Nach dem Klick auf "ok" wird die verlinkte JSON-Datei geladen. Dann KANN im Boardmanager der entsprechende Treiber geladen werden. Boardsmanager öffnen (`Tools/Board/BoardsManager`):

![Boards Manager öffnen (Ctrl-Shift-8)](images/esp32/open_boardmanager.png)

Im Boards-Manager nach dem zum genutzten Microcontrolller passenden Board suchen (z.B. "ESP32" für einfache ESPs) und die Boards installieren:

![Boards Manager öffnen (Ctrl-Shift-8)](images/esp32/install_board_esp32.png)

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="uno"  style="display:none">

Für den Arduino Uno bringt die Arduino-IDE bereits alle nötigen Treiber mit, so dass hier keine weitere Installation und Konfiguration nötig ist.

</span>


#### Port und Board

Der häufigste Fehler bei der Programmierung in der Arduino-IDE ist die Auswahl eines falschen Boards / Ports. Immer, wenn Fehler beim Übertragen eines Programms auftauchen, sollte überprüft werden, ob die folgenden Einstellungen stimmen.

##### An welchem Port hängt mein Microcontroller?

Unter Windows-Systemen wird dem Microcontroller ein COM-Port zugewiesen. Die zugewiesene Nummer weicht dabei ab, wenn unterschiedliche weitere Geräte angemeldet sind. Ein einfacher Weg, herauszufinden, welcher COM-Port zugewiesen wurde ist, das Board ein- und auszustecken und zu beobachten, welcher COM-Port betroffen ist.

In meinem Fall wurde dem Board `COM4` zugewiesen. Falls hier kein Board erscheint muss ggf. der USB-Treiber noch nachinstalliert werden.

In der IDE wird der entsprechende Port über `Tools/Port` eingetragen:

![Auswahl des COM-Ports, der dem NodeMCU zugewiesen wurde über Tools/Port](images/nodemcu/install-port.png)

Wer unsicher ist, welcher COM-Port betroffen ist - oder auf Fehler stößt und zusätzliche Infos benötigt - der kann z.B. im Gerätemanager oder mit Hilfe der PowerShell nach angeschlossenen USB-Devices suchen:

```powershell
Get-PnpDevice -Presentonly | Where-Object {($_.Class -like "Ports") -and ($_.PNPDeviceID -like "USB*")} | Format-Table -Property Class, Name, Description, PNPDeviceID
```

Aus der zugehörigen Ausgabe geht auch hervor, welcher USB-Chip verwendet wird, hier mal unterschiedliche Antworten, auf die ich später noch eingehe:

```
    Class Name                                          Description                            PNPDeviceID
    ----- ----                                          -----------                            -----------
(1) Ports Silicon Labs CP210x USB to UART Bridge (COM7) Silicon Labs CP210x USB to UART Bridge USB\VID_10C4&PID_EA60\0001
(2) Ports USB-SERIAL CH340 (COM6)                       USB-SERIAL CH340                       USB\VID_1A86&PID_7523\6&24E558FB&0&4
(3) Ports USB-Enhanced-SERIAL CH343 (COM3)              USB-Enhanced-SERIAL CH343              USB\VID_1A86&PID_55D3\5735020188
(4) Ports USB Serial Device (COM4)                      Serielles USB-Gerät                    USB\VID_303A&PID_1001&MI_00\7&84C9D3C&0&0000
(5) Ports USB Serial Device (COM9)                      Serielles USB-Gerät                    USB\VID_0483&PID_374B&MI_02\9&5000A63&0&0002
(6) Ports USB Serial Device (COM10)                     Serielles USB-Gerät                    USB\VID_239A&PID_0010&MI_02\9&212F6B11&0&0002
(7) Ports USB Serial Device (COM11)                     Serielles USB-Gerät                    USB\VID_239A&PID_0010&MI_00\9&212F6B11&0&0000
```

Wenn keine dieser Möglichkeiten ausgegeben wird, fehlt vermutlich ein Treiber. Ob überhaupt ein Chip erkannt wird, kann man im Gerätemanager nachschauen oder mit folgendem Powershell-Befehl:

```powershell
Get-PnpDevice -Presentonly | Where-Object {($_.PNPDeviceID -like "USB*")} | Format-Table -Property Class, Name, Description, PNPDeviceID
```

Hier sollte eine der oben abgebildeten Zeilen ausgegeben werden, allerdings ohne einen Eintrag in der Spalte "Class" (da ohne Treiber kein Port erkannt wird)

Am häufigsten ist das sicher der  CP210x (Zeile 1 oben), 



In meinem Fall wurde dem Board `COM4` zugewiesen. Sollte dem ESP kein Port zugewiesen werden (Port ist dann oft ausgegraut), dann ist möglicherweise der USB-Treiber für den verwendeten Chip nicht installiert (USB-zu UART-Bridge). Die Software ist schnell nachinstalliert - mit etwas Mut, denn die Herstellerseiten sind mitunter komplett chinesisch:

- Am häufigsten sind CP210x-Chips verbaut (Zeile 1). Die Treiber sind meistens bereits im System vorhanden. Falls nicht bietet deren Hersteller SiliconLabs auf [dieser Internetseite](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers) die Treiber an.

- Etwas seltener findet man CH340 oder ähnliche Chips (Zeile 2). Deren Treiber müssen meistens noch installiert werden. Der Hersteller bietet sie [auf dieser Seite (falls Darstellung chinesisch: unten CH341SER.EXE)](http://www.wch-ic.com/products/CH340.html)

- Neuere ESP32 benötigen gar keine expliziten USB-UART-Chips mehr, da diese ESP32 direkt per USB angesprochen werden können (Zeile 4 - 7).

- Falls das alles nicht hilft gibt es [auf esp32.net](http://esp32.net/usb-uart/) eine gute Übersicht der verwendeten Chips.


Sollte der Treiber für CHG340 oder CP210x fehlen, so muss dieser über die oben genannten Internetseiten geladen und entpackt werden. Danach kann im Gerätemanager das nicht zugeordnete Gerät ausgewählt werden:

![Im Geräte manager Gerät wählen und im Kontextmenü die Installation starten](images/nodemcu/install-Geraetemanager.png)

Über die manuelle Treiberinstallation kann der Pfad zum heruntergeladenen Treiber herausgesucht werden:

![Den Treiber manuell installieren](images/nodemcu/install-Geraetemanager-2.png)

Nach der Auswahl des Pfads wird der Treiber dann korrekt installiert und der Microcontroller gefunden.

![Pfad zum Treiber eingeben.](images/nodemcu/install-Geraetemanager-3.png)

In der IDE wird der entsprechende Port über `Tools/Port` eingetragen:

![Auswahl des COM-Ports, der dem NodeMCU zugewiesen wurde über Tools/Port](images/nodemcu/install-port.png)

##### Das korrekte Board wählen

Das zugehörige (und eben installierte) Board wird ebenso unter `Tools` eingetragen. Unter `Tools/Board/` finden sich Gruppen der Microcontroller. Hier muss das passende Board gefunden werden.

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="uno" onclick="openTabsByDataAttr('uno', 'mcu')">Arduino Uno</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

Unter `Tools/Board/esp8266` öffnet sich eine lange Liste:

![Das Board unter Tools/Board/esp8266 wählen](images/nodemcu/install-boardmanager-esp8266-04.png)

Aus dieser Liste sollte der neuste NodeMCU-Treiber gewählt werden:

![Aus der Liste "NodeMCU 1.0" auswählen](images/nodemcu/install-boardmanager-esp8266-05.png)

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">


![Das Board unter Tools/Board/esp32 wählen](images/esp32/board_esp32.png)

Bei den allermeisten ESP32-Boards findet sich auf dem Microcontroller die Bezeichnung des verbauten Chips - in Abhängigkeit davon muss das entsprechende Board gewählt werden. 

![Eine Auswahl an ESP32-Boards, 4x WROOM32, 1x ESP32-C6](images/esp32/board-uebersicht-boardtreiber.jpg)

- Häufig ist dies ein **ESP WROOM32**: dann muss `ESP32 Dev-Module` gewählt werden. Bei den meisten günstigsten Boards mit Micro-USB-Anschluss wird dies zutreffen.

- Weiter verbreitet sind auch **ESP32-C6-WROOM-1**-Boards (einige mit USB-C): dann muss `ESP32C6 Dev-Module` ausgewählt werden. Es erscheinen zwei Ports: für uns relevant ist verwirrenderweise derjenige Port, der **nicht** mit "ESP32 Family Device" benannt ist.

- Entsprechend muss bei den anderen ESP32-Chips verfahren werden (C3, H2, P4, S2 (...Natvie), S3 (..., Box, USB, CAM), WROOM_DA, Wrover (...Kit), Pico-D4): auf dem Chip steht die jeweilige Version, die auch als Board-Treiber gewählt werden muss.

- Einige ESP32 können nicht mit der Arduino-IDE programmiert werden (z.B. C2, C5, C61, P4)

Eine Übersicht über die Unterschiede findet sich z.B. [auf der Seite des Herstellers Espressif](https://www.espressif.com/en/products/modules) oder der englischen [Wikipediaseite zum ESP32](https://en.wikipedia.org/wiki/ESP32).

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="uno"  style="display:none">

Für Arduino UNO findet es sich unter `Tools/Board/Arduino AVR Boards/Arduino UNO`.

</span>

Damit ist alles Startbereit für den ersten _Sketch_!


#### Ablauf eines "Hello World" Programms

Einige Microcontroller haben bereits auf den Boards verbaute interne LED oder interne Buttons, die wir für ein erstes "Hello World" nutzen können.

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="uno" onclick="openTabsByDataAttr('uno', 'mcu')">Arduino Uno</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

Das _nodeMCU_-Board mit ESP8266 bietet uns folgende Möglichkeiten: Eine LED auf dem Microcontrollerboard, die mit dem Ausgang `D4` verschaltet ist, eine LED auf dem nodeMCU-Board, die mit dem Ausgang `D0` verschaltet ist und ein Taster ("FLASH"), der am Eingang D3 angeschlossen ist:

![Die Lage der internen LED und Taster auf dem nodeMCU-Board](images/Foto_interneLEDTaster.jpg)

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">

Nicht alle ESP32-Boards verfügen über Onboard-LEDs. Wir nutzen die intern verbauten LED und Taster des ESP32-Boards: nur beim DevKit v1 ist eine blaue LED auf dem Microcontrollerboard mit dem Ausgang `GPIO02` verschaltet, die meisten anderen ESP32 haben leider keine internen LED, die sich leicht ansprechen lassen. Beim _DevKit C V2_ gibt es zwar eine LED an `GPIO1`, da diese aber mit USB verbunden ist, spricht für den Anfang viel dafür, hier die Finger wegzunehmen... 

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="uno"  style="display:none">

Wir nutzen die intern verbauten LED des Arduino-Uno-Boards: Normalerweise ist eine LED auf dem Microcontrollerboard mit dem Ausgang `D13` verschaltet.

Sofern Du keinen der drei hier behandelten MCU hast, findest Du für AVR-basierte Microcontroller [hier die Pin-Bezeichnung der internen LED](https://docs.arduino.cc/built-in-examples/basics/Blink/).

</span>


Wir nutzen die Arduino-IDE, um diese internen LED und Taster anzusteuern. Ein Arduino-Sketch besteht im wesentlichen aus fünf Komponenten, von denen drei durch uns konfigurierbar sind:

- Nach dem Start wird eine Funktion `init()` aufgerufen, die alle nötigen Bibliotheken lädt und konfiguriert. Diese Methode wird uns nicht angezeigt und ist nicht konfigurierbar.

- Danach kommt ein Bereich, in dem wir eigene Variablen erzeugen können, die wir in unserem Sketch benötigen.

- Darauf folgt eine `setup`-Methode, in der wir alle Konfigurationen vornehmen können, die für unser Programm erforderlich sind. Die `setup()`-Methode wird genau einmal aufgerufen.

- Nach der `setup()`-Methode folgt eine Dauerschleife. Die Schleife selbst sehen wir nicht, wir wissen nur, dass in ihr eine weitere Methode aufgerufen wird:

- Die `loop()`-Methode befindet sich in der Dauerschleife. In der `loop()`-Methode befindet sich die eigentliche Programmlogik - entweder direkt, oder in Form von Funktionsaufrufen.

Grafisch lässt sich das ganze als Programm-Ablaufplan (PAP) darstellen:^[Grafik wurde erstellt mit dem PAP-Designer von Friedrich Folkmann: [http://friedrich-folkmann.de/papdesigner/Hauptseite.html](http://friedrich-folkmann.de/papdesigner/Hauptseite.html)]

![PAP: auf init und setup folgt die Schleife, in der loop immer wieder aufgerufen wird](images/PAP_des_Hauptprogramms.png)


#### Der eigentliche Programmcode

Wir können die verbauten LED mit ein paar Zeilen Code ein- und ausschalten:

Zunächst legen wir die Variable fest für den _General Purpose Input Output_ (GPIO), an dem die LED angeschlossen ist.  Dazu definieren wir eine Variable, die den Anschluss der jeweiligen LED speichert. Dadurch ist das Programm später leichter änderbar, da wir nur an einer einzigen Stelle erfassen, wo sich die LED befindet.

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="uno" onclick="openTabsByDataAttr('uno', 'mcu')">Arduino Uno</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

Eine interne LED ist am ESP-12 über 470 Ohm mit dem GPIO `D4` verbunden, die nutzen wir für unser Ausgangsbeispiel. 

![Lage der internen ESP-12-LED](images/Foto_interneLED_ESP.jpg)

```cpp
/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin       | Name, Verhalten*/
const int ONBOARD_LED        = D4;           // interne LED 
```

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">

Wenn Du in der glücklichen Lage bist, dass Dein Board an `GPIO1` oder `GPIO2` über eine interne LED verfügt, dann kannst Du folgendes probieren: 

```cpp
/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int ONBOARD_LED        = 1;       // Interne LED GPIO01 HIGH-Aktiv
```

Falls Du über keine interne LED verfügst, kannst Du [hier](https://oer-informatik.de/externe_led_ansteuern_mcu) nachlesen, wie Du eine LED anschließen kannst.

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="uno"  style="display:none">

Häufig gibt es zwei LED: eine, die signalisiert, wenn am seriellen Port (also über USB) Daten übertragen werden. Diese liegt am `D0` an. Wir nutzen eine zweite LED, die am `D13` angeschlossen ist.

```cpp
/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int ONBOARD_LED        = D13;     // Interne LED
```

</span>


In der `setup()`-Methode werden alle Operationen aufgerufen, die genau ein einziges Mal beim Starten ausgeführt werden müssen. Hier wird konfiguriert, dass es sich bei dem Pin, den wir gerade ind er Variable `ONBOARD_LED` gespeichert haben, um ein Ausgabedevice (also eine LED aus Ausgabe) handelt:

```c++
void setup() {
  pinMode(ONBOARD_LED, OUTPUT); 
}
```

In einer zweiten Methode, die in Arduino-Sketches immer vorkommt, werden alle Operationen aufgerufen, die in einer Dauerschleife laufen sollen. Hier wird an den Ausgabepin `ONBOARD_LED` abwechselnd ein `HIGH`-  und ein `LOW`-Potenzial angelegt. Ein `HIGH`-Potenzial heißt, dass der Microcontroller an diesem Pin 5V (bei Arduino Uno) bzw. 3,3V (bei ESP8266 bzw. ESP32)  anlegt, ein `LOW` entpricht einem Potenzial von 0V. Die LED sollte also blinken.

```c++
void loop() {
    digitalWrite(ONBOARD_LED, HIGH);
    delay(200);
    digitalWrite(ONBOARD_LED, LOW);
    delay(800);
}
```

Das Potenzial wird kurz auf `HIGH` und lange auf `LOW` gelegt - wir erwarten also ein kurzes Leuchten (200 Millisekunden) und eine lange Pause (800 Millisekunden). 

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="uno" onclick="openTabsByDataAttr('uno', 'mcu')">Arduino Uno</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

Es ist genau umgekehrt: lange Pause, kurzes Leuchten! Um das Verhalten zu verstehen müssen wir uns die interne Schaltung ansehen:

![Schaltplan der LED des ESP8266-Chips](images/nodeMCU_interneLED_ESP.png)

Sobald an `D4` ein `LOW`-Signal anliegt ergibt sich eine Spannungs-Potenzialdifferenz (3,3V - 0V), die zu einem Stromfluss durch die LED führt. Liegt an `D4` ein `HIGH`-Potenzial an, so ergibt sich keine Potenzialdifferenz (beide Seiten auf 3,3V) und die LED leuchtet nicht. Die LED verhält sich also gerade umgekehrt, als man es erwarten würde: sie ist bei einem anliegenden `LOW`-Potenzial aktiv, man nennt dies auch _low-active_.

Es gibt eine zweite LED am nodeMCU-Board, zwischen dem "RST"-Taster und den gelben Bauelementen:

![Lage der internen NodeMCU-LED](images/Foto_interneLED_nodeMCU.jpg)

Diese LED ist genauso an `D0` verschaltet wie die vorige LED an `D4`:

![Die interne LED des nodeMCU-Boards ist über 470 Ohm an D0 verschaltet](images/nodeMCU_interneLED_node.png)

Wir können daher alle Überlegungen von oben übernehmen, auch das Programm sieht genauso aus, wir müssen lediglich `D4` durch `D0` ersetzen:

```cpp
/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin       | Name, Verhalten*/
const int ONBOARD_LED        = D0;            // interne LED
```

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">

Wenn Dein Board onboard-LED hat, dann kannst Du genau das beobachten: Die LED scheint also zwischen dem `ONBOARD_LED` Pin und Ground (0v) verschaltet zu sein. Wenn sie leuchtet, sobald ein `HIGH`-Signal anliegt, spricht man von einem "HIGH"-aktiven Ausgang. Wäre die LED gegen 3,3V verschaltet, so würde sie leuchten, sobald ein `LOW`-Signal am Ausgang anliegt (das ist z.B. beim ESP8266 der Fall). In diesem Fall spricht man von `LOW`-aktiv.

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="uno"  style="display:none">

So leuchtet die LED auch. Die LED scheint also zwischen dem `ONBOARD_LED` Pin und Ground (0v) verschaltet zu sein. Wenn sie leuchtet, sobald ein `HIGH`-Signal anliegt, spricht man von einem "HIGH"-aktiven Ausgang. Wäre die LED gegen 5V verschaltet, so würde sie leuchten, sobald ein `LOW`-Signal am Ausgang anliegt (das ist z.B. beim ESP8266 der Fall). In diesem Fall spricht man von `LOW`-aktiv.

</span>


#### Interne Taster der Microcontroller

<span class="tabrow" >
  <button class="tablink tabselected" data-tabgroup="mcu" data-tabid="esp8266" onclick="openTabsByDataAttr('esp8266', 'mcu')">ESP8266 (nodeMCU)</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="esp32" onclick="openTabsByDataAttr('esp32', 'mcu')">ESP32</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="uno" onclick="openTabsByDataAttr('uno', 'mcu')">Arduino Uno</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="all" onclick="openTabsByDataAttr('all', 'mcu')">_all_</button>
  <button class="tablink" data-tabgroup="mcu" data-tabid="none" onclick="openTabsByDataAttr('none', 'mcu')">_none_</button>
</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp8266">

Der `FLASH`-Taster, der neben dem Micro-USB-Port sitzt, lässt sich ebenfalls direkt für Programme nutzen. Er darf nur beim Booten des ESP-Boards nicht betätigt sein, danach kann er frei genutzt werden:

![Schaltplan der LED des ESP8266-Chips](images/Foto_internerTaster.jpg)

Intern ist er mit dem `D3`-Eingang verschaltet und verfügt über einen _Pull-Up_-Widerstand. Das heißt: wenn der Taster nicht betätigt ist, liegt ein `HIGH`-Signal an `D3` an.

![Die interne LED des nodeMCU-Boards ist über 470 Ohm an D0 verschaltet](images/nodeMCU_internerTaster.png)

Bei Betätigung liegt an `D3` eine sehr niedrige Spannung an, die als `LOW`-Signal interpretiert wird. Es sind nicht exakt `0 V`, da zwischen `D3` und `GND` noch ein $470\ \Omega$ Widerstand liegt. Die Potenzialdifferenz zwischen `GND` (0V) und `3V3` (3,3V) teilt sich im Verhältnis der Widerstände auf, an `D3` liegen also 0,11V an:

$$
U_{D3} = 3,3 V \cdot \frac{430 \Omega}{12.000 \Omega + 430 \Omega} = 0,11 V
$$

Gemäß Datenblatt erkennt der ESP8266 Eingangssignale zwischen $-0,3V$ und $0,8 V$ gesichert als `LOW`, wir sind somit also auf der sicheren Seite. Der Arduino-Sketch ist folgendermaßen aufgebaut:

- Wir deklarieren eine neue Variable für den internen Taster: `const int ONBOARD_BUTTON ` und weisen ihr den Wert `D4` zu.

- In der `setup()`-Methode wird dieser Pin als Eingang festgelegt: `pinMode(ONBOARD_BUTTON, INPUT);`. Die LED an Pin `D4` wird wieder als Ausgang genutzt.

- In der Dauerschleife (`loop()`) wird ausgelesen, ob das anliegende Potenzial an `D3` ein `LOW`-Pegel ist: `if (digitalRead(ONBOARD_BUTTON)==LOW){...}`.

- Bei anliegendem `LOW`-Pegel wird die Zeit, die die LED aktiviert wird auf kurz eingestellt (200 Millisekunden), sonst auf einen längeren Wert (1200 Millisekunden).


Im Ganzen sieht das so aus:

```c++
/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int ONBOARD_LED        = D4;          // interne LED
const int ONBOARD_BUTTON     = D3;          // D3 Taster, Pullup
```

Den so festgelegten Pin müssen wir jetzt noch als Eingang deklarieren:

```c++
void setup() {
  pinMode(ONBOARD_LED, OUTPUT);
  pinMode(ONBOARD_BUTTON, INPUT_PULLUP);  
}
```

Und schließlich in der Programmschliefe den Zustand des Button-Pins abfragen (mit `digitalRead()`). Wenn der Eingang des Pins `LOW` ist, soll die LED schnell blinken. Andernfalls soll sie langsam blinken:

```c++
void loop() {
  int miliSecondsLow=0;

  if (digitalRead(ONBOARD_BUTTON)==LOW){
    miliSecondsLow = 200;
  }else{
    miliSecondsLow = 1200;
  }
    digitalWrite(ONBOARD_LED, HIGH);
    delay(200);
    digitalWrite(ONBOARD_LED, LOW);
    delay(miliSecondsLow);
}
```

Nach dem Hochladen kann man erkennen, dass die LED normalerweise langsam blinkt (der Pins also regulär `HIGH` ist) und nur bei Betätigung des Buttons langsam wird (also `LOW`). Dieses Verhalten liegt an einem intern verbauten "Pull-Up-Widerstand". Dazu später mehr, wenn wir uns die Belegung von externen Tastern genauer anschauen.

</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="esp32"  style="display:none">


Ohne weitere Bauteile können wir uns der zweiten großen Aufgabe stellen: digitale Signale einzulesen. Dazu nutzen wir eine weitere Komponente, die auf den meisten NodeMCU-Boards mit ESP32 verbaut ist: der Boot-Button. Er ist direkt mit dem digitalen Eingang `GPIO0` verbunden und wird normalerweise genutzt, um den ESP beim Start in den Programmiermodus zu setzen.

Wir nutzen diesen Button nach dem Booten anderweitig:

Bei betätigtem Button soll sich die Geschwindigkeit des Blinkens ändern. Damit wir auch etwas sehen, wenn keine onboard-LED verfügbar ist, geben wir auf dem seriellen Monitor eine Meldung aus. 

Dazu brauchen wir zunächst in der Liste der Ein- und Ausgabeelemente eine zusätzliche Zeile für den Taster:

```c++
/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int ONBOARD_LED        = 1;           // GPIO01 HIGH-Aktiv
const int ONBOARD_BUTTON     = 0;           // GPIO00, Pullup
```

Den so festgelegten Pin müssen wir jetzt noch als Eingang deklarieren:

```c++
void setup() {
  Serial.begin(115200);  // Seriellen Monitor in IDE anzeigen per Strg-Shift-M und 115200baud einstellen!
  pinMode(ONBOARD_LED, OUTPUT);
  pinMode(ONBOARD_BUTTON, INPUT_PULLUP);  
}
```

Und schließlich in der Programmschliefe den Zustand des Button-Pins abfragen (mit `digitalRead()`). Wenn der Eingang des Pins `LOW` ist, soll die LED schnell blinken. Andernfalls soll sie langsam blinken:

```c++
void loop() {
  int miliSecondsLow=0;

  if (digitalRead(ONBOARD_BUTTON)==LOW){
    Serial.print("Taster nicht betätigt");
    miliSecondsLow = 200;
  }else{
    miliSecondsLow = 1200;
    Serial.print("Taster betätigt");
  }
    digitalWrite(ONBOARD_LED, HIGH);
    Serial.print("LED an");
    delay(200);
    digitalWrite(ONBOARD_LED, LOW);
    Serial.print("LED aus");
    delay(miliSecondsLow);
}
```

Nach dem Hochladen kann man erkennen, dass die LED normalerweise langsam blinkt (der Pins also regulär `HIGH` ist) und nur bei Betätigung des Buttons langsam wird (also `LOW`). Dieses Verhalten liegt an einem intern verbauten "Pull-Up-Widerstand". Dazu später mehr, wenn wir uns die Belegung von externen Tastern genauer anschauen.


</span>

<span class="tabs" data-tabgroup="mcu" data-tabid="uno"  style="display:none">

Leider verfügt der Arduino Uno über keine internen Taster, die genutzt werden können. Um mit Eingaben zu experimentieren, müssen wir also auf [externe Taster](https://oer-informatik.de/taster_einlesen_nodemcu) zurückgreifen.

</span>



#### Wo befinden sich die versteckten Programmbestandteile?

Für alle, die es genau wissen wollen (alle anderen können diesen Absatz getrost überspringen):

Eingangs wurde erwähnt, dass es über die beiden oben genannten Funktionen `setup()` und `loop()` hinaus noch weiteren Programmcode gibt, den die Arduino-IDE aber vor uns versteckt. Gibt es eine Möglichkeit danach zu forschen? Gibt es beispielsweise eine `main()`-Methode, wie sie die _Java_ und _C/C++_ Programmierer kennen?

Ja, die gibt es, man kann sie tief im Arduino-Ordner aufspüren, bei den 1.8.x-Versionen der Arduino-IDE in Windows liegt sie beispielsweise hier:

```bash
C:\
  Program Files\
   arduino-x.x.x\
     hardware\
       arduino\
         avr\
           cores\
             arduino\
               main.cpp
```

In dieser `main.cpp`-Datei befindet sich die `main()`-Methode des C++-Programms. Und darin befinden sich die eingangs erwähnten Methodenaufrufe (siehe Programmablaufplan oben) `init()`, `setup()` und `loop()`, sowie die Dauerschleife (hier über `for (;;) {`...}` umgesetzt):

```cpp
int main(void)
{
	init();

	initVariant();

#if defined(USBCON)
	USBDevice.attach();
#endif

	setup();

	for (;;) {
		loop();
		if (serialEventRun) serialEventRun();
	}

	return 0;
}

```

Für den ESP ist dieser Aufruf nicht ganz so einfach aufzuspüren - und das umgebende Programm etwas aufwändiger. Der Grundaufbau ist aber identisch: `setup()` wird einmal aufgerufen, `loop()` immer wieder.  

#### Wie geht es weiter?

Weiter geht es im zweiten Teil, in dem [digitale Ausgänge genutzt und externe LED angesteuert werden](https://oer-informatik.de/externe_led_ansteuern_nodemcu).

#### Weitere Literatur und Quellen

* Es führt kein Weg an der [Arduino Referenz](https://www.arduino.cc/reference/en/) vorbei, der Dokumentation für die internen Befehle des Arduino-Frameworks

* ESP8266: Primärquelle ist immer das [Datenblatt und die API-Dokumentation von Espressif (dem Chiphersteller)](https://www.espressif.com/en/support/documents/technical-documents?keys=8266&field_type_tid[]=14)

* ESP32: Primärquelle ist immer das [Datenblatt und die API-Dokumentation von Espressif (dem Chiphersteller)](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/index.html)

