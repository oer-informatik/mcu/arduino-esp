//-------------------------------------------------------------------------------------
// Logging to serial console? 
// If following line is commentet ("//#define DEBUG") all logging-operations will be
// replaced by "", otherwise if "#define DEBUG" is present logging will be sent to serial 
//-------------------------------------------------------------------------------------

#define DEBUG  //Flag to activate logging to serial console (i.e. serial monitor in arduino ide)

#ifdef DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif


//-------------------------------------------------------------------------------------
// LogLevels used in this example. Only entries bigger than LOG_LEVEL will be written
//-------------------------------------------------------------------------------------

String LOG_LEVEL_NAMES[] = {"OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"};
const int MIN_LOG_LEVEL = 5;

int i; // iterating numbers in example sketch... can be deleted...

void setup(){
  #ifdef DEBUG
  Serial.begin(115200); // Activate debugging via serial monitor
  #endif
}

void loop(){
  i++;
  debugOutput("Logging No. "+String(i), 4);
  delay(1000);
}

void debugOutput(String text, int logLevel) {
  if (MIN_LOG_LEVEL >= logLevel) {
    DEBUG_PRINTLN("["+LOG_LEVEL_NAMES[logLevel]+ "] " + text);
  }
}