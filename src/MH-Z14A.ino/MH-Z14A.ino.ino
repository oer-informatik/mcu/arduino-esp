#include <SoftwareSerial.h>

/*Liste der Ein- und Ausgabelemente  
Datentyp | Name       | Anschlusspin   | Name, Verhalten
*/
const int PIN_RX          = 7;//___;

const int PIN_TX          = 8;//___;

const int PIN_ANALOGIN    = A0;//___;

const int PIN_PWM         = 9;//___;

SoftwareSerial SerialCom(PIN_RX, PIN_TX);

void setup() {
  SerialCom.begin(9600);
  Serial.begin(9600);
  pinMode(PIN_PWM, INPUT_PULLUP);
  delay(1000); // preheat the CO2 sensor for 3 minutes
  Serial.println("Analog:,UART:,PWM:");
}

void loop() {
  int ppm_analog = get_analog();
  int ppm_uart = gas_concentration_uart();
  int ppm_PWM = gas_concentration_PWM();
  Serial.print(ppm_analog);
  Serial.print(",");
  Serial.print(ppm_uart);
  Serial.print(",");
  Serial.println(ppm_PWM);
  delay(1000); // sleep for 1 second
  }

int get_analog() {
  float v = analogRead(PIN_ANALOGIN) * 5.0 / 1023.0;
  int gas_concentration = int((v) * (5000/2));
  return gas_concentration;
}

int gas_concentration_uart() {
  byte addArray[] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
  byte dataValue[9];
  SerialCom.write(addArray, 9);
  SerialCom.readBytes(dataValue, 9);
  int resHigh = (int) dataValue[2];
  int resLow  = (int) dataValue[3];
  int ppm_uart = (resHigh*256)+resLow;
  return ppm_uart;
}

int gas_concentration_PWM() {
  while (digitalRead(PIN_PWM) == LOW) 
    {delay(0);};
  long t0 = millis();
  while (digitalRead(PIN_PWM) == HIGH) 
    {delay(0);};
  long t1 = millis();
  while (digitalRead(PIN_PWM) == LOW) 
    {delay(0);};
  long t2 = millis();
  long tH = t1-t0;
  long tL = t2-t1;
  long ppm = 5000L * (tH - 2) / (tH + tL - 4);
  while (digitalRead(PIN_PWM) == HIGH) 
    {delay(0);};
  delay(10);
  return int(ppm);
}