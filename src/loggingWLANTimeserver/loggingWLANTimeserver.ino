
//-------------------------------------------------------------------------------------
// Load WiFi-Libraries depending on Hardware (ESP32/ESP8266) using preprocessor directives
//-------------------------------------------------------------------------------------

#if defined(ESP8266)
#pragma message "Compiling Libraries for ESP8266-based boards"
#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;
#elif defined(ESP32)
#pragma message "Compiling Libraries for ESP32-based boards"
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#else
#error "Nor ESP32 or ESP8266 recognized - you have to choose the libraries manually"
#endif


//-------------------------------------------------------------------------------------
// All Credentials are stored in file secrets.h (must be created using secrets_EXAMPLE.h)
// => add line "*secrets.h" to .gitignore to prevent publishing credentials to repository
//-------------------------------------------------------------------------------------

#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol and sharing


//-------------------------------------------------------------------------------------
// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------

const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;               // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                  // Reboot ESP after __ times connection errors


//-------------------------------------------------------------------------------------
// Configuration of the NTP-Server
//-------------------------------------------------------------------------------------

#include "time.h"
const char* NTP_SERVER = "pool.ntp.org";
const long GMT_OFFSET_SEC = 3600;
const int DAYLIGHT_OFFSET_SEC = 3600;


//-------------------------------------------------------------------------------------
// Logging to serial console? 
// If following line is commentet ("//#define DEBUG") all logging-operations will be
// replaced by "", otherwise if "#define DEBUG" is present logging will be sent to serial 
//-------------------------------------------------------------------------------------

#define DEBUG  //Flag to activate logging to serial console (i.e. serial monitor in arduino ide)

#ifdef DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif


//-------------------------------------------------------------------------------------
// LogLevels used in this example. Only entries bigger than LOG_LEVEL will be written
//-------------------------------------------------------------------------------------

String LOG_LEVEL_NAMES[] = {"OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"};
const int MIN_LOG_LEVEL = 5;

int i; // iterating numbers in example sketch... can be deleted...

void setup(){
  #ifdef DEBUG
  Serial.begin(115200); // Activate debugging via serial monitor
  #endif
  WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
  ensureWIFIConnection();                    // Call connection-function for the first 
  
   // Init and get the time
  configTime(GMT_OFFSET_SEC, DAYLIGHT_OFFSET_SEC, NTP_SERVER);
}

void loop(){
  ensureWIFIConnection();    
  i++;
  debugOutput("Logging No. "+String(i), 4);
  delay(1000);
}

void debugOutput(String text, int logLevel) {
  if (MIN_LOG_LEVEL >= logLevel) {
    String timeAsString = "";    
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
      timeAsString = "[no NTP]";
    }else{
      char timeAsChar[20];
      strftime(timeAsChar, 20, "%Y-%m-%d_%H:%M:%S", &timeinfo);
      timeAsString = String(timeAsChar);
    }
    DEBUG_PRINTLN("["+timeAsString + "] ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text);
  }
}

void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
       debugOutput("No WIFI Connection found. Re-establishing...", 3;
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
         debugOutput("WLAN Connection attempt number " + String(connectionRetry), 4);
        if (connectionRetry > MAX_CONNECTION_RETRY) {
           debugOutput("Connection Failed! Rebooting...", 4);
          delay(5000);
          ESP.restart();
        }
      }
      debugOutput("WiFi is connected", 4, true);
      debugOutput("IP address: " + (WiFi.localIP().toString()), 4);
      debugOutput("Connected to (SSID): " + String(WiFi.SSID()), 5);
      debugOutput("Signal strength (RSSI): " + String(WiFi.RSSI()) + "(-50 = perfect / -100 no signal)", 5);
    }
  }