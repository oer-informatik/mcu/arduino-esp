#if defined(ESP8266)
#pragma message "Compiling Libraries for ESP8266-based boards"
#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;
#elif defined(ESP32)
#pragma message "Compiling Libraries for ESP32-based boards"
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#else
#error "Nor ESP32 or ESP8266 recognized - you have to choose the libraries manually"
#endif

#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol / sharing


// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;                // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                   // Reboot ESP after __ times connection errors


void setup(){
  Serial.begin(115200);                      // Activate debugging via serial monitor
  WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
  ensureWIFIConnection();                    // Call connection-function for the first time
}

 void loop() {
    ensureWIFIConnection();                  // make sure, WiFi is still alive, reboot if necessary 
  }

   void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
      Serial.println("No WIFI Connection found. Re-establishing...");
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
        Serial.println("WLAN Connection attempt number " + String(connectionRetry));
        if (connectionRetry > MAX_CONNECTION_RETRY) {
          Serial.println("Connection Failed! Rebooting...");
          delay(5000);
          ESP.restart();
        }
      }
      Serial.println("WiFi is connected");
      Serial.println("IP address: " + (WiFi.localIP().toString()));
      Serial.println("Connected to (SSID): " + String(WiFi.SSID()));
      Serial.println("Signal strength (RSSI): " + String(WiFi.RSSI()) + " dB (-50 dB = perfect / -100 dB no signal)");
    }
  }