# 1 "C:\\Users\\hanne\\Desktop\\Blink\\Blink.ino"


void setup() {
  pinMode(13, 0x1);
  pinMode(19, 0x2);
}

void loop() {
  if (digitalRead(19) == 0x1) {
    digitalWrite(13, 0x1); // turn the LED on (HIGH is the voltage level)
    delay(1000); // wait for a second
    digitalWrite(13, 0x0); // turn the LED off by making the voltage LOW
    delay(1000); // wait for a second
  }
}
