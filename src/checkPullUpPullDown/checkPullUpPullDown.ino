void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  int pinList[] = { 0, 2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 };
  int pinAmount = (sizeof(pinList) / sizeof(pinList[0]));
  for (int i = 0; i < pinAmount; i++) {
    Serial.print("Checking GPIO");
    Serial.print(pinList[i]);
    Serial.print(": ");
    pinMode(pinList[i], INPUT_PULLUP);
    delay(0);
    if (digitalRead(pinList[i]) == HIGH) {
      Serial.print(" pullup ok ");
    } else {
      Serial.print(" pullup FAIL !!!!!!!!!");
    }
    delay(0);
    pinMode(pinList[i], INPUT_PULLDOWN);
    delay(0);
    if (digitalRead(pinList[i]) == LOW) {
      Serial.print(" /  pulldown ok ");
    } else {
      Serial.print(" pulldown FAIL !!!!!!!!!");
    }
    Serial.println(" |");
  }
}
