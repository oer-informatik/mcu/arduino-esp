#include <Adafruit_BME280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     MCU_SDA             = 21;               // GPIO of MCU connected to SDA-Pin of Sensor
const int     MCU_SCL             = 22;               // GPIO of MCU connected to SDCL-Pin of Sensor
const int     I2C_ADDR            = 0x76;             // if SD0 is floating or LOW 0x76, else 0x77

//-------------------------------------------------------------------------------------
// App-Settings
//-------------------------------------------------------------------------------------
#define SEALEVELPRESSURE_HPA (1013.25)
unsigned long delayTime             = 1000;
unsigned long serialMonitorBaudrate = 115200;

Adafruit_BME280 bme; 


void setup(){
  Serial.begin(serialMonitorBaudrate);        // Activate debugging via serial monitor
  
  Wire.begin(MCU_SDA, MCU_SCL);               // connection with individual I2C-Pins
  int status = bme.begin(I2C_ADDR, &Wire);    //status = bme.begin();
  
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
    Serial.print("SensorID was: 0x"); 
    Serial.println(bme.sensorID(),HEX);
    while (1) delay(10);
  }
  Serial.println("|Temperature | Pressure   | Altitude | Humidity |");
}


void loop() { 
    serialPrintMeasurement();
    delay(delayTime);
}


void serialPrintMeasurement() {
  char prBuffer[51]; // Print buffer
  float temp     = bme.readTemperature();
  float pressure = bme.readPressure() / 100.0F;
  float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  float humidity = bme.readHumidity();

  sprintf(prBuffer, "|    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |",temp, pressure, altitude, humidity);
  Serial.println(prBuffer);
}