#include <Adafruit_BME280.h>

Adafruit_BME280 bme; 

void setup(){
  Serial.begin(115200);
  if (!bme.begin(0x76)) {Serial.println("Could not find a valid BME280 sensor, check wiring, i2c address");}
  Serial.println("Temperature | Pressure   | Altitude | Humidity |");
}

void loop() { 
  char textBuffer[48];
  sprintf(textBuffer, "    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f", bme.readTemperature(), (bme.readPressure() / 100.0F), bme.readAltitude(1013.25), bme.readHumidity());
  Serial.println(String(textBuffer) + String(" % |"));
  delay(1000);
}