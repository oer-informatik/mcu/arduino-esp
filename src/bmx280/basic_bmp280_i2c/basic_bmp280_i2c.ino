#include <Adafruit_BME280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     BMP_CS              = 5;                // GPIO of MCU connected to SPI-Chip-Select-(CSB)-Pin

Adafruit_BME280 bme(BMP_CS); // hardware SPI

void setup(){
  Serial.begin(115200);
  if (!bme.begin()) {Serial.println("Could not find a valid BME280 sensor, check wiring");}
  Serial.println("Temperature | Pressure   | Altitude | Humidity |");
}

void loop() { 
  char textBuffer[52];
  sprintf(textBuffer, "    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |", bme.readTemperature(), (bme.readPressure() / 100.0F), bme.readAltitude(1013.25), bme.readHumidity());
  Serial.println(textBuffer));
  delay(1000);
}