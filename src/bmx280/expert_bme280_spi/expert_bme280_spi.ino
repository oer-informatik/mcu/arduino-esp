#include <Adafruit_BME280.h>

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     BMP_CS              = 5;                // GPIO of MCU connected to SPI-Chip-Select-(CSB)-Pin
const int     BMP_MOSI            = 23;               // GPIO of MCU connected to SDA/SPI-MOSI-Pin
const int     BMP_MISO            = 19;               // GPIO of MCU connected to SDO/SPI-MISO-Pin
const int     BMP_CLK             = 18;               // GPIO of MCU connected to SCL/SPI-Clock-Pin

//-------------------------------------------------------------------------------------
// App-Settings
//-------------------------------------------------------------------------------------
#define SEALEVELPRESSURE_HPA (1013.25)
unsigned long delayTime             = 1000;
unsigned long serialMonitorBaudrate = 115200;

Adafruit_BME280 bme(BMP_CS, BMP_MOSI, BMP_MISO, BMP_CLK); // indivudual SPI-Pins

void setup(){
  Serial.begin(serialMonitorBaudrate);        // Activate debugging via serial monitor

  int status = bme.begin();
  
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring");
    while (1) delay(10);
  }
  Serial.println("|Temperature | Pressure   | Altitude | Humidity |");
}


void loop() { 
    serialPrintMeasurement();
    delay(delayTime);
}


void serialPrintMeasurement() {
  char prBuffer[51]; // Print buffer
  float temp     = bme.readTemperature();
  float pressure = bme.readPressure() / 100.0F;
  float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  float humidity = bme.readHumidity();

  sprintf(prBuffer, "|    %2.1f °C | %4.1f hPa |   %3.1f m |   %2.1f %% |",temp, pressure, altitude, humidity);
  Serial.println(prBuffer);
}