#pragma once                                             // Only run once, even if included multiple times


#define SECRET_WIFI_SSID     "meinWLAN";         // Wifi network name (SSID)
#define SECRET_WIFI_PASSWORD "1234567890123456"; // Wifi network password

#define SECRET_OTA_HOSTNAME "MeinESPProjekt";             // Name of device for over-the-air-updates (OTA)
#define SECRET_OTA_PASSWORD "superIndividuellesPasswort"; // Password for over-the-air-updates (OTA)