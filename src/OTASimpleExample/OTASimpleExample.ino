#if defined(ESP8266)
#pragma message "Compiling Libraries for ESP8266-based boards"
#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;
#elif defined(ESP32)
#pragma message "Compiling Libraries for ESP32-based boards"
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#else
#error "Nor ESP32 or ESP8266 recognized - you have to choose the libraries manually"
#endif

#include <ArduinoOTA.h>
#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol / sharing

// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;               // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                  // Reboot ESP after __ times connection errors


//-------------------------------------------------------------------------------------
// Over-the-Air Update (Upload new Software via WiFi)
// OTA-Password-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------
const char* OTA_HOSTNAME = SECRET_OTA_HOSTNAME; // Name of device for over-the-air-updates (OTA)
const char* OTA_PASSWORD = SECRET_OTA_PASSWORD; // Password for over-the-air-updates (OTA)

//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_UPDATE_ACTIVE   = 4;               // HIGH = Update active

const bool ENABLE_UPDATE_JUMPER = false;

void setup(){
  Serial.begin(115200);                      // Activate debugging via serial monitor
  WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
  ensureWIFIConnection();                    // Call connection-function for the first time
  startOTA();
  //...
}


void loop() {
  ensureWIFIConnection();                  // make sure, WiFi is still alive, reboot if necessary

  if ((digitalRead(PIN_UPDATE_ACTIVE) == HIGH)||(!ENABLE_UPDATE_JUMPER)){
    ArduinoOTA.handle();
  }else{
    //... normaler Programmablauf, der im Updatemodus nicht ausgeführt werden soll
  }
  //... normaler Programmablauf, der auch im Updatemodus ausgeführt werden soll
}

void startOTA() {
  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else  // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    });
  
  ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
    });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.println("Progress: " + String(progress / (total / 100)));
    });
  
  ArduinoOTA.onError([](ota_error_t error) {
      Serial.println("Error " + String(error));
      if (error == OTA_AUTH_ERROR)         Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR)   Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR)     Serial.println("End Failed");
    });

  ArduinoOTA.setHostname(OTA_HOSTNAME);
  ArduinoOTA.setPassword(OTA_PASSWORD);
  ArduinoOTA.begin();
}

  void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
      Serial.println("No WIFI Connection found. Re-establishing...");
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
        Serial.println("WLAN Connection attempt number " + String(connectionRetry));
        if (connectionRetry > MAX_CONNECTION_RETRY) {
          Serial.println("Connection Failed! Rebooting...");
          delay(5000);
          ESP.restart();
        }
      }
      Serial.println("WiFi is connected");
      Serial.println("IP address: " + (WiFi.localIP().toString()));
      Serial.println("Connected to (SSID): " + String(WiFi.SSID()));
      Serial.println("Signal strength (RSSI): " + String(WiFi.RSSI()) + " dB (-50 dB = perfect / -100 dB no signal)");
    }
  }