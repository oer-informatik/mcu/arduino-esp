void setup() {
  Serial.begin(115200);
      Serial.println("Neustart ");
      delay(200);   
}

void loop() {
  bool buttonstate = HIGH;
  for (int i=0; i<34; i++){

    Serial.print("Ausgabe von GPIO Nr "+String(i)+":");
    
    if (((i>=6)&&(i<=11))){
      Serial.println(" SD-Pins: keine Prüfung...");
      continue;
    }else{
      delay(20);
      Serial.print(" LED an ");
      if (i==1){
        Serial.end();
        delay(20);
      }
      pinMode(i, OUTPUT);
      digitalWrite(i, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(500);                      // wait for a second
      digitalWrite(i, LOW);   // turn the LED off by making the voltage LOW
      if (i==1){
        Serial.begin(115200);
      }
      Serial.print(" / aus ");
      delay(500);
      if (i>0){
        pinMode(0, INPUT_PULLUP);
        buttonstate = digitalRead(0);
      }
      Serial.print(" - Button "+String(buttonstate));
      Serial.println("...");
  }    
  }                  // wait for a second
}
