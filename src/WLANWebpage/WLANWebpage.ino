/*
Basic 
*/

//-------------------------------------------------------------------------------------
// Set the Port for the Webserver
//-------------------------------------------------------------------------------------

const int WEBSERVER_PORT = 80;

//-------------------------------------------------------------------------------------
// Load WiFi and Webserver-Libraries depending on Hardware (ESP32/ESP8266) using preprocessor directives
//-------------------------------------------------------------------------------------

#if defined(ESP8266)
#pragma message "Compiling Libraries for ESP8266-based boards"

#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;

#include <ESP8266WebServer.h> // aktivieren für ESP8266
ESP8266WebServer server(WEBSERVER_PORT);

#elif defined(ESP32)
#pragma message "Compiling Libraries for ESP32-based boards"
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;

#include <WebServer.h>
WebServer server(WEBSERVER_PORT);

#else
#error "Nor ESP32 or ESP8266 recognized - you have to choose the libraries manually"
#endif

//-------------------------------------------------------------------------------------
// All Credentials are stored in file secrets.h (must be created using secrets_EXAMPLE.h)
// => add line "*secrets.h" to .gitignore to prevent publishing credentials to repository
//-------------------------------------------------------------------------------------

#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol and sharing


//-------------------------------------------------------------------------------------
// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------

const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;               // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                  // Reboot ESP after __ times connection errors


void setup(){

  Serial.begin(115200); // Activate debugging via serial monitor
  Serial.println("WiFi will be established");
  WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
  Serial.println("Connecting to a WiFi Accesspoint");
  ensureWIFIConnection();                    // Call connection-function for the first 
  
  Serial.println("Starting Webserver...");
  server.on("/", homepage);
  String log_address = "http://"+WiFi.localIP().toString() + ":" + String(WEBSERVER_PORT);
  Serial.println("Logging will be published on: "+log_address );
  server.begin();
  Serial.println("Finished startup.");
}

void loop(){
  ensureWIFIConnection(); 
  server.handleClient(); 
  
  // START LOGGING EXAMPLE... REPLACE WITH YOUR OWN CODE

  // START LOGGING EXAMPLE... REPLACE WITH YOUR OWN CODE
}

void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
       Serial.println("No WIFI Connection found. Re-establishing...", 3, true);
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
         Serial.println("WLAN Connection attempt number " + String(connectionRetry), 4, true);
        if (connectionRetry > MAX_CONNECTION_RETRY) {
           Serial.println("Connection Failed! Rebooting...");
          delay(5000);
          ESP.restart();
        }
      }
      Serial.println("WiFi is connected");
      Serial.println("IP address: " + (WiFi.localIP().toString()));
      Serial.println("Connected to (SSID): " + String(WiFi.SSID()));
      Serial.println("Signal strength (RSSI): " + String(WiFi.RSSI()) + "(-50 = perfect / -100 no signal)");
    }
  }


String renderHtml(String header, String body) {
  // HTML & CSS contents which display on web server
  String html = "";
  html = html + "<!DOCTYPE html>\n<html>\n"+"<html lang='de'>\n<head>\n<meta charset='utf-8'>\n<title>"+header+"</title>\n</head><body>\n<h1>";
  html = html + header + "</h1>\n";
  html = html + body + "\n</body>\n</html>\n";
  return html;
}

void homepage() {
  String header = "Debugging-Log-Entries";
  String body = "";
  body = "<h2>Logging on Startup / during configuration (Setup-Log)</h2>\n";
  body = body + setupLogText;
  body = body + "<h2>Logging during operation (Loop-Log)</h2>\n";
  body = body + loopLogText;
  server.send(200, "text/html; charset=utf-8", renderHtml(header, body));
}