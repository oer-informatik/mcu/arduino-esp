// Portangaben beziehen sich auf einen Arduino Micro (AtMega 32u4)

/*
  Liste der Ein- und Ausgabelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int MAX_LOAD = 19;  // ChipSelect, invertiert (LOW-Aktiv)
const int MAX_CLK = 21;   // Clock
const int MAX_DIN = 20;   // Dateneingang des Displays

int delaytime_us = 1;

void setup() {
  pinMode(MAX_LOAD, OUTPUT);
  pinMode(MAX_CLK, OUTPUT);
  pinMode(MAX_DIN, OUTPUT);
  digitalWrite(MAX_LOAD, LOW);
  digitalWrite(MAX_CLK, LOW);

  poweronMatrix(true);
  matrixScanlimit(7);
  matrixIntensity(15);
  testMatrix(true);
  delay(500);
  testMatrix(false);
  matrixDecode(false);
  delay(500);
  Serial.begin(57600);
}

void shiftByte(byte inputByte) {
  for (int myBitNr = 0; myBitNr < 8; myBitNr++) {
    bool myBit = bitRead(inputByte, 7 - myBitNr);
    digitalWrite(MAX_DIN, myBit);
    delayMicroseconds(delaytime_us);
    digitalWrite(MAX_CLK, HIGH);
    delayMicroseconds(delaytime_us);
    digitalWrite(MAX_CLK, LOW);
  }
}


void shiftBinaryWord(byte address, byte data) {
  digitalWrite(MAX_LOAD, LOW);
  shiftByte(address);
  shiftByte(data);
  digitalWrite(MAX_LOAD, HIGH);
  delayMicroseconds(delaytime_us);
  Serial.print("Befehl: ");
  Serial.println(address);
  Serial.print("Daten: ");
  Serial.println(data);
}

void loop() {
  byte x_byte = 0b00000001;
/*
  for (int y = 1; y < 9; y++) {
    x_byte = 0b00000001;
    for (int x = 0; x < 9; x++) {
      shiftBinaryWord(y, x_byte);
      delay(200);
      x_byte = x_byte << 1;
    }
  }
  
  displayByte(0b01010101, 1);
  displayByte(0b10101010, 2);
  displayByte(0b00110011, 0);
  */
byte bitMuster2[] = {0b00111100,
                    0b01000010,
                    0b10000001,
                    0b10000001,
                    0b10000001,
                    0b10000001,
                    0b01000010,
                    0b00111100};

byte bitMuster[] = {0b00011000,
                    0b00100100,
                    0b01000010,
                    0b10000001,
                    0b10000001,
                    0b10000001,
                    0b00100100,
                    0b00011000,};

  byte myFont[8][128];
  myFont['A'] = {0b00011000,
                 0b00100100,
                 0b00100100,
                 0b01000010,
                 0b01111100,
                 0b10000001,
                 0b10000001,
                 0b10000001
                 };                  
  displayScreen(myFont['A']);
  delay(1000);
}

void poweronMatrix(bool isSwitchedOn) {
  shiftBinaryWord(0b00001100, isSwitchedOn);
}

void matrixScanlimit(byte limit) {
  shiftBinaryWord(0b00001011, limit);
}

void testMatrix(bool isTestOn) {
  shiftBinaryWord(0b00001111, isTestOn);
}

void matrixDecode(bool isDecodeOn) {
  if (isDecodeOn) {
    shiftBinaryWord(0b00001001, 0b11111111);
  } else {
    shiftBinaryWord(0b00001001, 0b00000000);
  }
}

void matrixIntensity(byte intensity) {
  shiftBinaryWord(0b00001010, intensity);
}

void displayByte(byte bitMuster, byte col) {
  if ((col>0) and (col<=8)) { // nur zulässige Befehle
    shiftBinaryWord(col, bitMuster);
  }
}

void displayScreen(byte bitMuster[]) {
  for (byte col = 1; col <= 8; col = col + 1) {
    displayByte(bitMuster[col-1], col);
  }
}
