/*  Liste der Ein- und Ausgabeelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int ONBOARD_LED        = 1;           // GPIO01 HIGH-Aktiv
const int ONBOARD_BUTTON     = 0;           // GPIO00, Pullup

void setup() {
  pinMode(ONBOARD_LED, OUTPUT);
  pinMode(ONBOARD_BUTTON, INPUT_PULLUP);  
}


void loop() {
  bool buttonstate =digitalRead(ONBOARD_BUTTON);

  if (buttonstate==LOW){
    digitalWrite(ONBOARD_LED, HIGH); // switch LED on
  }else{
    digitalWrite(ONBOARD_LED, LOW);  // switch LED off
  }
   
  delay(100);
}