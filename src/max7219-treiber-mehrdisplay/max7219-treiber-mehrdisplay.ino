/*
  Liste der Ein- und Ausgabelemente
Datentyp |Name           | Anschlusspin | Name, Verhalten*/
const int MAX_LOAD       = 19;          // ChipSelect, invertiert (LOW-Aktiv)
const int MAX_CLK        = 21;          // Clock
const int MAX_DIN        = 20;          // Dateneingang des Displays

int delaytime_us = 100;
byte display_count = 1;

void setup() {
  pinMode(MAX_LOAD, OUTPUT);
  pinMode(MAX_CLK, OUTPUT);
  pinMode(MAX_DIN, OUTPUT);
  digitalWrite(MAX_LOAD, LOW);
  digitalWrite(MAX_CLK, LOW);
  
  poweron_matrix(true);
  scanlimit_matrix(7);
  matrix_intensity(15);
  test_matrix(true);
  delay(500);
  test_matrix(false);
  decode_matrix(false);
  delay(500);
}

void shiftByte(byte inputByte){
  for (int myBitNr = 0; myBitNr<8; myBitNr++){
    bool myBit = bitRead(inputByte, 7-myBitNr);
    digitalWrite(MAX_DIN,myBit);
    digitalWrite(MAX_CLK,HIGH);
    delayMicroseconds(delaytime_us);
    digitalWrite(MAX_CLK,LOW);
  }
}


void shiftBinaryWord(byte address, byte data){
  shiftBinaryWord(address, data, 0);
}

void shiftBinaryWord(byte address, byte data, byte display_nr){
  digitalWrite(MAX_LOAD,LOW);
  shiftByte(address);
  shiftByte(data);
  for (int i = 0; i<display_nr; i++){
    shiftByte(0b00000000);
    shiftByte(0b00000000);
  }
  digitalWrite(MAX_LOAD,HIGH);
  delayMicroseconds(delaytime_us);

}

void loop() {
    byte x_byte = 0b00000001;
    
    for (int y = 1; y<9; y++){
      x_byte = 0b00000001;
      for (int x = 0; x<9; x++){
        shiftBinaryWord(y, x_byte, 1);
        delay(200);
        x_byte = x_byte << 1;  
      }
    
    }
    //shiftBinaryWord(0b00000000, 0b00000000);
}

void poweron_matrix(bool isSwitchedOn){
    for (int i=0; i<display_count; i++){
      shiftBinaryWord(0b00001100, isSwitchedOn, i);
    }
  }

void scanlimit_matrix(byte limit){
      for (int i=0; i<display_count; i++){
          shiftBinaryWord(0b00001011, limit, i);
    }

  }
    
void test_matrix(bool isTestOn){
    for (int i=0; i<display_count; i++){
      shiftBinaryWord(0b00001111, isTestOn, i);
    }
    shiftBinaryWord(0b00001111, isTestOn);
  }
  
void decode_matrix(bool isTestOn){
  if (isTestOn){
      shiftBinaryWord(0b00001001, 0b11111111);
  }else{
      shiftBinaryWord(0b00001001, 0b00000000);
  }
  }
  
void matrix_intensity(byte intensity){
    for (int i=0; i<display_count; i++){
      shiftBinaryWord(0b00001010, intensity, i);
    }
  }

