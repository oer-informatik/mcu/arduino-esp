//-------------------------------------------------------------------------------------
// Load WiFi-Libraries depending on Hardware (ESP32/ESP8266) using preprocessor directives
//-------------------------------------------------------------------------------------

#if defined(ESP8266)
#pragma message "Compiling Libraries for ESP8266-based boards"
#include <ESP8266WiFiMulti.h> // aktivieren für ESP8266
ESP8266WiFiMulti wifiMulti;
#elif defined(ESP32)
#pragma message "Compiling Libraries for ESP32-based boards"
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#else
#error "Nor ESP32 or ESP8266 recognized - you have to choose the libraries manually"
#endif

//-------------------------------------------------------------------------------------
// All Credentials are stored in file secrets.h (must be created using secrets_EXAMPLE.h)
// => add line "*secrets.h" to .gitignore to prevent publishing credentials to repository
//-------------------------------------------------------------------------------------

#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol and sharing


//-------------------------------------------------------------------------------------
// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------

const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;               // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                  // Reboot ESP after __ times connection errors



//-------------------------------------------------------------------------------------
// Configuration of the NTP-Server
//-------------------------------------------------------------------------------------

#include "time.h"
const char* NTP_SERVER = "pool.ntp.org";
const long GMT_OFFSET_SEC = 3600;
const int DAYLIGHT_OFFSET_SEC = 3600;



//-------------------------------------------------------------------------------------
// Set Route and Port for the Logpage-Webserver
//-------------------------------------------------------------------------------------

#include <WebServer.h>
const int WEBSERVER_PORT = 8085;
const char* WEBSERVER_ROUTE_TO_DEBUG_OUTPUT = "/log";

WebServer server(WEBSERVER_PORT);
String setupLogText = "";
String loopLogText = "";

//-------------------------------------------------------------------------------------
// Logging to serial console? 
// If following line is commentet ("//#define DEBUG") all logging-operations will be
// replaced by "", otherwise if "#define DEBUG" is present logging will be sent to serial 
//-------------------------------------------------------------------------------------

#define DEBUG  //Flag to activate logging to serial console (i.e. serial monitor in arduino ide)

#ifdef DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif


//-------------------------------------------------------------------------------------
// Over-the-Air Update (Upload new Software via WiFi)
// OTA-Password-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------
#include <ArduinoOTA.h>

const char* OTA_HOSTNAME = SECRET_OTA_HOSTNAME; // Name of device for over-the-air-updates (OTA)
const char* OTA_PASSWORD = SECRET_OTA_PASSWORD; // Password for over-the-air-updates (OTA)

const bool ENABLE_UPDATE_JUMPER = false;


//-------------------------------------------------------------------------------------
// LogLevels used in this example. Only entries bigger than LOG_LEVEL will be written
//-------------------------------------------------------------------------------------

String LOG_LEVEL_NAMES[] = {"OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"};
const int MIN_LOG_LEVEL = 7;

int i; // iterating numbers in example sketch... can be deleted...


//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_UPDATE_ACTIVE   = 4;               // HIGH = Update active



void setup(){

  // START LOGGING EXAMPLE... REPLACE WITH YOUR OWN CODE
  debugOutput("Starting Programm...", 6, true);
  #ifdef DEBUG
  Serial.begin(115200); // Activate debugging via serial monitor
  #endif
  debugOutput("WiFi will be established", 6, true);
  WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
  debugOutput("Connecting to a WiFi Accesspoint", 5, true);
  ensureWIFIConnection();                    // Call connection-function for the first 
  
   // Init and get the time
  debugOutput("Connection to NTP-Timeserver", 6, true);
  configTime(GMT_OFFSET_SEC, DAYLIGHT_OFFSET_SEC, NTP_SERVER);

  debugOutput("Starting Webserver...", 6, true);
  server.on(WEBSERVER_ROUTE_TO_DEBUG_OUTPUT, respondRequestWithLogEntries);
  String log_address = "http://"+WiFi.localIP().toString() + ":" + String(WEBSERVER_PORT) + WEBSERVER_ROUTE_TO_DEBUG_OUTPUT;
  debugOutput("Logging will be published on: "+log_address , 5, true);
  server.begin();

  startOTA();

  debugOutput("Finished startup.", 6, true);
}

void loop(){
  ensureWIFIConnection();
  if ((digitalRead(PIN_UPDATE_ACTIVE) == HIGH)||(!ENABLE_UPDATE_JUMPER)){
    ArduinoOTA.handle();
  }
  server.handleClient();   
  // START LOGGING EXAMPLE... REPLACE WITH YOUR OWN CODE
  debugOutput("Logging Beispieleintrag Nr. "+String(i), 4, false);
  delay(1000);
  // START LOGGING EXAMPLE... REPLACE WITH YOUR OWN CODE
}

void debugOutput(String text, int logLevel, bool setupLog) {
  if (MIN_LOG_LEVEL >= logLevel) {
    String timeAsString = "";    
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
        timeAsString = "[no NTP]";
    }else{
      char timeAsChar[20];
      strftime(timeAsChar, 20, "%Y-%m-%d_%H:%M:%S", &timeinfo);
      timeAsString = String(timeAsChar);
    }
    if (setupLog) {
      setupLogText = setupLogText + "[" +  timeAsString + "] " + " ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text + "<br/>\n";
    } else {
      loopLogText = loopLogText + "[" +  timeAsString + "] " + " ["+LOG_LEVEL_NAMES[logLevel]+ "] "+ text + "<br/>\n";
    }
    DEBUG_PRINTLN("["+timeAsString + "] ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text);
  }
}


void debugOutput(String text, int logLevel) {
  debugOutput(text, logLevel, false); // log to loopLogText is default
}


void debugOutput(String text) {
  debugOutput(text, 4); // no loglevel present? use "INFO"
}


void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
       debugOutput("No WIFI Connection found. Re-establishing...", 3, true);
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
         debugOutput("WLAN Connection attempt number " + String(connectionRetry), 4, true);
        if (connectionRetry > MAX_CONNECTION_RETRY) {
           debugOutput("Connection Failed! Rebooting...", 4, true);
          delay(5000);
          ESP.restart();
        }
      }
      debugOutput("WiFi is connected", 4, true);
      debugOutput("IP address: " + (WiFi.localIP().toString()), 4, true);
      debugOutput("Connected to (SSID): " + String(WiFi.SSID()), 5, true);
      debugOutput("Signal strength (RSSI): " + String(WiFi.RSSI()) + "(-50 = perfect / -100 no signal)", 5, true);
    }
  }


String renderHtml(String header, String body) {
  // HTML & CSS contents which display on web server
  String html = "";
  html = html + "<!DOCTYPE html>\n<html>\n"+"<html lang='de'>\n<head>\n<meta charset='utf-8'>\n<title>"+header+"</title>\n</head><body>\n<h1>";
  html = html + header + "</h1>\n";
  html = html + body + "\n</body>\n</html>\n";
  return html;
}

void respondRequestWithLogEntries() {
  String header = "Debugging-Log-Entries";
  String body = "";
  body = "<h2>Logging on Startup / during configuration (Setup-Log)</h2>\n";
  body = body + setupLogText;
  body = body + "<h2>Logging during operation (Loop-Log)</h2>\n";
  body = body + loopLogText;
  server.send(200, "text/html; charset=utf-8", renderHtml(header, body));
}

void startOTA() {
  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else  // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      debugOutput("Start OTA Firmware update " + type, 4);
    });

  ArduinoOTA.onEnd([]() {
      debugOutput("\nEnd OTA Firmware update ", 4);
    });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      debugOutput("OTA Progress: " + String(progress / (total / 100)), 4);
    });

  ArduinoOTA.onError([](ota_error_t error) {
      debugOutput("Error " + String(error), 4);
      if (error == OTA_AUTH_ERROR)         debugOutput("Auth Failed", 2);
      else if (error == OTA_BEGIN_ERROR)   debugOutput("Begin Failed", 2);
      else if (error == OTA_CONNECT_ERROR) debugOutput("Connect Failed", 2);
      else if (error == OTA_RECEIVE_ERROR) debugOutput("Receive Failed", 2);
      else if (error == OTA_END_ERROR)     debugOutput("End Failed", 2);
    });

  ArduinoOTA.setHostname(OTA_HOSTNAME);
  ArduinoOTA.setPassword(OTA_PASSWORD);
  ArduinoOTA.begin();
}